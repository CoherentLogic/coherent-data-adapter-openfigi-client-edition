package com.coherentlogic.coherent.data.adapter.openfigi.core.builders;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * 
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Disabled
public class QueryBuilderTest {

    @BeforeEach
    public void setUp() throws Exception {
    }

    @AfterEach
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetRequestBody() {
        fail("Not yet implemented");
    }

    @Test
    public void testGetHeaders() {
        fail("Not yet implemented");
    }

    @Test
    public void testWithApiKey() {
        fail("Not yet implemented");
    }
}
