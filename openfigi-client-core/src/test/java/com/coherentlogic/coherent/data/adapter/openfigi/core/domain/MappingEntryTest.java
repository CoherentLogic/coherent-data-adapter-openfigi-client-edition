package com.coherentlogic.coherent.data.adapter.openfigi.core.domain;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.MappingEntry;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.RequestBody;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.exceptions.ConstraintViolationException;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.exceptions.InvalidMappingEntryConfigurationException;

/**
 * Unit test for the {@link MappingEntry} class.
 */
public class MappingEntryTest {

    private static final String TEST = "test";

    private MappingEntry mappingEntry = null;

    @BeforeEach
    public void setUp() throws Exception {
        mappingEntry = new MappingEntry(new RequestBody (null));
    }

    @AfterEach
    public void tearDown() throws Exception {
        mappingEntry = null;
    }

    @Test
    public void testSetExchangeCodeToNonNullValue() {
        mappingEntry.setExchangeCode(TEST);
    }

    @Test
    public void testSetExchangeCodeToNullValue() {
        mappingEntry.setExchangeCode((String) null);
    }

    @Test
    public void testSetExchangeCodeWithMisconfiguration() {
        assertThrows(InvalidMappingEntryConfigurationException.class, () -> {
            mappingEntry.setMarketIdentificationCode(TEST);
            mappingEntry.setExchangeCode(TEST);
        });
    }

    @Test
    public void testSetMarketIdentificationCodeToNonNullValue() {
        mappingEntry.setMarketIdentificationCode(TEST);
    }

    @Test
    public void testSetMarketIdentificationCodeToNullValue() {
        mappingEntry.setMarketIdentificationCode((String) null);
    }

    @Test
    public void testSetMarketIdentificationCodeWithMisconfiguration() {
        assertThrows(InvalidMappingEntryConfigurationException.class, () -> {
            mappingEntry.setExchangeCode(TEST);
            mappingEntry.setMarketIdentificationCode(TEST);
        });
    }
}
