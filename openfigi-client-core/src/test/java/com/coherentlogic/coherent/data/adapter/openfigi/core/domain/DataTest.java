package com.coherentlogic.coherent.data.adapter.openfigi.core.domain;

import java.util.function.Consumer;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.Data;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

public class DataTest {

	@Disabled("Disabled until EDA issue # 10 has been addressed -- see here: https://bitbucket.org/CoherentLogic/coherent-logic-enterprise-data-adapter/issues/10/the-defaulttraversalstrategy-needs-to")
    @Test
    public void testAcceptNullConsumer () {

        Data data = new Data ();

        Consumer<SerializableBean> consumer = null;

        data.accept(consumer);
    }

    /**
     * This should work and no exception should be thrown.
     */
    @Test
    public void testAcceptCollectionOfConsumerOfSerializableBean() {

        Data data = new Data ();

        Consumer<SerializableBean>[] consumers = new Consumer[] {};

        data.accept(consumers);
    }
}
