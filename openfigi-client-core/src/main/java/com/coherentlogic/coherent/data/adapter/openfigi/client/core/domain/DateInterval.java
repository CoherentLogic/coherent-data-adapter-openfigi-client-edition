package com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * [
 *     {"idType":"BASE_TICKER","idValue":"FG","marketSecDes":"Mtge","securityType2":"Pool","maturity":["2019-09-01","2020-06-01"]
 * ]
 * 12:02:01 [
 *     {"idType": "TICKER", "idValue": "COF6C 55", "securityType2":"Option","expiration":[null, "2025-12-26"]
 * ]
 *
 * -----
 * # curl 'https://api.openfigi.com/v3/mapping' --request POST --header 'Content-Type: application/json' --data '[{"idType":"ID_WERTPAPIER","idValue":"851399","exchCode":"US", "includeUnlistedEquities":true, "securityType2":"Option"}]'
 *
 * expiration : Array(2) / When securityType2 is Option or Warrant / Will find instruments whose expiration date falls in an interval. Value should be an Array interval of the form [a, b] where a, b are date Strings or null. Date strings must be of the form YYYY-MM-DD. At least one entry must be a date String. When both are date Strings, it is required that a and b are no more than one year apart. Also, [a, null] is equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
 *
 * // maturity : Array(2) / When securityType2 is Pool / Will find instruments whose maturity date falls in an interval. Value should be an Array interval of the form [a, b] where a, b are date Strings or null. Date strings must be of the form YYYY-MM-DD. At least one entry must be a date String. When both are date Strings, it is required that a and b are no more than one year apart. Also, [a, null] is equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
 *
 * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class DateInterval extends AbstractInterval<String> {

    private static final DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public DateInterval(String aValue) {
        super(aValue);
    }

    public DateInterval(String aValue, String bValue) {
        super(aValue, bValue);
    }

    public DateInterval(Date aValue) {
        super(simpleDateFormat.format(aValue));
    }

    public DateInterval(Date aValue, Date bValue) {
        super(simpleDateFormat.format(aValue), simpleDateFormat.format(bValue));
    }

    /**
     * When both are date Strings, it is required that a and b are no more than one year apart.
     */
    @Override
    public DateInterval validate() {
        /* At the moment we're not validating the dates and will push this responsibility onto the server.
         *
         * When date is not less than one year use this text (for maturity):
         *
         * "Maturity range must span less than 1 year."
         */
        return this;
    }
}
