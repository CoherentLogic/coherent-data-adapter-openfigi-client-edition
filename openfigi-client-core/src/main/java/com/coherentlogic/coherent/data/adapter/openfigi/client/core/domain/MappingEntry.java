package com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain;

import static com.coherentlogic.coherent.data.model.core.util.Utils.assertNotNull;

import java.util.Date;

import javax.persistence.Transient;

import com.coherentlogic.coherent.data.adapter.openfigi.client.core.exceptions.InvalidMappingEntryConfigurationException;
import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * A mapping request entry.
 *
 * Refer to the {@link QueryBuilder} for an example that demonstrates how this class is used.
 *
 * @see <a href="https://openfigi.com/api#post-v3-mapping">OpenFIGI POST /v3/mapping</a>
 * @see <a href="https://openfigi.com/assets/local/figi-allocation-rules.pdf">FIGI Allocation Rules</a>
 *
 * @todo Consider renaming this class to MappingRequestEntry possibly.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class MappingEntry {

    /*
     * The code for the various enumerations was generated using the Groovy script below.
     *
     * String values = '''stuff here'''
     *
     * values.eachLine {
     *   println "    ${it.replace (' ', '_').replace ('-', '').replace ('.', '').replace ('/', '_').replace ('&', 'AND').replace ('%', '_PCT')} (\"$it\"),"
     * }
     */

    public static final String OPEN_FIGI_MAPPING_ENTRY = "openFIGIMappingEntry";

    /**
     * @deprecated Move to an enum.
     */
    public static final String ID_ISIN = "ID_ISIN",
        ID_BB_UNIQUE = "ID_BB_UNIQUE",
        ID_SEDOL = "ID_SEDOL",
        ID_COMMON = "ID_COMMON",
        ID_WERTPAPIER = "ID_WERTPAPIER",
        ID_CUSIP = "ID_CUSIP",
        ID_CINS = "ID_CINS", // new
        ID_BB = "ID_BB",
        ID_ITALY = "ID_ITALY",
        ID_EXCH_SYMBOL = "ID_EXCH_SYMBOL",
        ID_FULL_EXCHANGE_SYMBOL = "ID_FULL_EXCHANGE_SYMBOL",
        COMPOSITE_ID_BB_GLOBAL = "COMPOSITE_ID_BB_GLOBAL",
        ID_BB_GLOBAL_SHARE_CLASS_LEVEL = "ID_BB_GLOBAL_SHARE_CLASS_LEVEL",
        ID_BB_SEC_NUM_DES = "ID_BB_SEC_NUM_DES",
        ID_BB_GLOBAL = "ID_BB_GLOBAL",
        TICKER = "TICKER",
        BASE_TICKER = "BASE_TICKER",
        ID_CUSIP_8_CHR = "ID_CUSIP_8_CHR",
        OCC_SYMBOL = "OCC_SYMBOL",
        UNIQUE_ID_FUT_OPT = "UNIQUE_ID_FUT_OPT",
        OPRA_SYMBOL = "OPRA_SYMBOL",
        TRADING_SYSTEM_IDENTIFIER = "TRADING_SYSTEM_IDENTIFIER",
        ID_SHORT_CODE = "ID_SHORT_CODE"; // new

    /**
     * Type of third party identifier. See Supported Identifiers for all supported 3rd party identifier types.
     *
     * @TODO Add "as" methods and/or possibly an enumeration.
     */
    @Expose
    private String idType = null;

    /**
     * The value for the represented third party identifier.
     */
    @Expose
    private String idValue = null;

    static final String
        EXCH_CODE = "exchCode",
        MIC_CODE = "micCode",
        MARKET_SEC_DES = "marketSecDes";

    /**
     * An optional exchange code if it applies (cannot use with micCode).
     */
    @Expose
    @SerializedName(EXCH_CODE)
    private String exchangeCode = null;

    /**
     * AKA micCode; an optional ISO market identification code(MIC) if it applies(cannot use with exchCode).
     */
    @Expose
    @SerializedName(MIC_CODE)
    private String marketIdentificationCode= null;

    /**
     * An optional currency if it applies.
     */
    @Expose
    private String currency = null;

    /**
     * An optional market sector description if it applies.
     */
    @Expose
    @SerializedName(MARKET_SEC_DES)
    private String marketSectorDescription = null;

 // -----

    static final String SECURITY_TYPE = "securityType";

    /**
     * Security type of the desired instrument(s). Click
     * <a href="https://openfigi.com/api/enumValues/v3/securityType">here</a> for the current list of securityType
     * values.
     *
     * @since 2.0.3.1-RELEASE
     */
    @Expose
    private String securityType;

    /**
     * Getter method for the security type.
     *
     * Security type of the desired instrument(s). Click
     * <a href="https://openfigi.com/api/enumValues/v3/securityType">here</a> for the current list of securityType
     * values.
     *
     * @since 2.0.3.1-RELEASE
     */
    public String getSecurityType() {
        return securityType;
    }

    /**
     * Setter method for the security type.
     *
     * Security type of the desired instrument(s). Click
     * <a href="https://openfigi.com/api/enumValues/v3/securityType">here</a> for the current list of securityType
     * values.
     *
     * @since 2.0.3.1-RELEASE
     */
    public void setSecurityType(@Changeable(SECURITY_TYPE) String securityType) {
        this.securityType = securityType;
    }

    /**
     * Setter method for the security type.
     *
     * Security type of the desired instrument(s). Click
     * <a href="https://openfigi.com/api/enumValues/v3/securityType">here</a> for the current list of securityType
     * values.
     *
     * @since 2.0.3.1-RELEASE
     */
    public void setSecurityType(SecurityType securityType) {
        setSecurityType (securityType.toString());
    }

    /**
     * Setter method for the security type.
     *
     * Security type of the desired instrument(s). Click
     * <a href="https://openfigi.com/api/enumValues/v3/securityType">here</a> for the current list of securityType
     * values.
     *
     * @since 2.0.3.1-RELEASE
     */
    public MappingEntry withSecurityType(String securityType) {

        setSecurityType(securityType);

        return this;
    }

    /**
     * Setter method for the security type.
     *
     * Security type of the desired instrument(s). Click
     * <a href="https://openfigi.com/api/enumValues/v3/securityType">here</a> for the current list of securityType
     * values.
     *
     * @since 2.0.3.1-RELEASE
     */
    public MappingEntry withSecurityType(SecurityType securityType) {
        return withSecurityType (securityType.toString());
    }

    static final String SECURITY_TYPE_2 = "securityType2";

    /**
     * Required when idType is BASE_TICKER or ID_EXCH_SYMBOL.
     *
     * An alternative security type of the desired instrument(s). securityType2 is typically less specific than
     * securityType. Click here for the current list of securityType2 values.
     *
     * @since 2.0.3.1-RELEASE
     */
    @Expose
    private String securityType2;

    /**
     * Required when idType is BASE_TICKER or ID_EXCH_SYMBOL.
     *
     * An alternative security type of the desired instrument(s). securityType2 is typically less specific than
     * securityType. Click here for the current list of securityType2 values.
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/securityType2">OpenFIGI securityType2 values</a>
     *
     * @since 2.0.3.1-RELEASE
     */
    public String getSecurityType2() {
        return securityType2;
    }

    /**
     * Required when idType is BASE_TICKER or ID_EXCH_SYMBOL.
     *
     * An alternative security type of the desired instrument(s). securityType2 is typically less specific than
     * securityType. Click here for the current list of securityType2 values.
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/securityType2">OpenFIGI securityType2 values</a>
     *
     * @since 2.0.3.1-RELEASE
     */
    public void setSecurityType2(@Changeable(SECURITY_TYPE_2) String securityType2) {
        this.securityType2 = securityType2;
    }

    /**
     * Required when idType is BASE_TICKER or ID_EXCH_SYMBOL.
     *
     * An alternative security type of the desired instrument(s). securityType2 is typically less specific than
     * securityType. Click here for the current list of securityType2 values.
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/securityType2">OpenFIGI securityType2 values</a>
     *
     * @since 2.0.3.1-RELEASE
     */
    public void setSecurityType2(SecurityType2 securityType2) {
        this.securityType2 = securityType2.toString();
    }

    /**
     * Required when idType is BASE_TICKER or ID_EXCH_SYMBOL.
     *
     * An alternative security type of the desired instrument(s). securityType2 is typically less specific than
     * securityType. Click here for the current list of securityType2 values.
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/securityType2">OpenFIGI securityType2 values</a>
     *
     * @since 2.0.3.1-RELEASE
     */
    public MappingEntry withSecurityType2(String securityType2) {

        setSecurityType2(securityType2);

        return this;
    }

    /**
     * Required when idType is BASE_TICKER or ID_EXCH_SYMBOL.
     *
     * An alternative security type of the desired instrument(s). securityType2 is typically less specific than
     * securityType. Click here for the current list of securityType2 values.
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/securityType2">OpenFIGI securityType2 values</a>
     *
     * @since 2.0.3.1-RELEASE
     */
    public MappingEntry withSecurityType2(SecurityType2 securityType2) {
        return withSecurityType2 (securityType2.toString());
    }

    private static final String INCLUDE_UNLISTED_EQUITIES = "includeUnlistedEquities";

    /**
     * Set to true to include equity instruments that are not listed on an exchange.
     *
     * curl 'https://api.openfigi.com/v3/mapping' --request POST --header 'Content-Type: application/json' --data '[{"idType":"ID_WERTPAPIER","idValue":"851399","exchCode":"US", "includeUnlistedEquities":true}]
     *
     * @since 2.0.3.1-RELEASE
     */
    @Expose
    @SerializedName(INCLUDE_UNLISTED_EQUITIES)
    private Boolean includeUnlistedEquities;

    /**
     * Set to true to include equity instruments that are not listed on an exchange.
     *
     * @return True to include equity instruments that are not listed on an exchange.
     *
     * @since 2.0.3.1-RELEASE
     */
    public Boolean getIncludeUnlistedEquities() {
        return includeUnlistedEquities;
    }

    /**
     * Set to true to include equity instruments that are not listed on an exchange.
     *
     * # curl 'https://api.openfigi.com/v3/mapping' --request POST --header 'Content-Type: application/json' --data '[{"idType":"ID_WERTPAPIER","idValue":"851399","exchCode":"US", "includeUnlistedEquities":true}]'
     *
     * @since 2.0.3.1-RELEASE
     */
    public void setIncludeUnlistedEquities(@Changeable(INCLUDE_UNLISTED_EQUITIES) Boolean includeUnlistedEquities) {
        this.includeUnlistedEquities = includeUnlistedEquities;
    }

    /**
     * Set to true to include equity instruments that are not listed on an exchange.
     *
     * curl 'https://api.openfigi.com/v3/mapping' --request POST --header 'Content-Type: application/json' --data '[{"idType":"ID_WERTPAPIER","idValue":"851399","exchCode":"US", "includeUnlistedEquities":true}]
     *
     * @since 2.0.3.1-RELEASE
     */
    public MappingEntry withIncludeUnlistedEquities(Boolean includeUnlistedEquities) {

        setIncludeUnlistedEquities(includeUnlistedEquities);

        return this;
    }

    private static final String OPTION_TYPE = "optionType";

    /**
     * Will filter instruments based on option type. Valid values are Call and Put.
     *
     * @since 2.0.3.1-RELEASE
     */
    @Expose
    @SerializedName(OPTION_TYPE)
    private String optionType;

    /**
     * Getter method for the optionType.
     *
     * Will filter instruments based on option type. Valid values are Call and Put.
     *
     * @since 2.0.3.1-RELEASE
     */
    public String getOptionType() {
        return optionType;
    }

    /**
     * Setter method for the optionType.
     *
     * Will filter instruments based on option type. Valid values are Call and Put.
     *
     * @since 2.0.3.1-RELEASE
     */
    public void setOptionType(@Changeable(OPTION_TYPE) String optionType) {
        this.optionType = optionType;
    }

    /**
     * Will filter instruments based on option type. Valid values are Call and Put.
     *
     * @since 2.0.3.1-RELEASE
     * 
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withOptionType (String optionType) {

        setOptionType(optionType);

        return this;
    }

    /**
     * Will filter instruments based on option type. Valid values are Call and Put.
     *
     * @since 2.0.3.1-RELEASE
     * 
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withOptionType (OptionType optionType) {
        return withOptionType (optionType.toString());
    }

    /**
     * Will filter instruments based on the Call option type.
     *
     * @since 2.0.3.1-RELEASE
     * 
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withOptionTypeAsCall () {
        return withOptionType (OptionType.Call);
    }

    /**
     * Will filter instruments based on the Put option type.
     *
     * @since 2.0.3.1-RELEASE
     * 
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withOptionTypeAsPut () {
        return withOptionType (OptionType.Put);
    }

    private static final String STRIKE = "strike";

    /**
     * Will find instruments whose strike price falls in an interval.
     *
     * Value should be an Array interval of the form [a, b] where a, b are Numbers or null. At least one entry must be a
     * Number. When both are Numbers, it is required that a <= b.
     * 
     * Also, [a, null] is equivalent to the interval [a, infinity) and [null, b] is equivalent to (-infinity, b].
     *
     * @since 2.0.3.1-RELEASE
     */
    @Expose
    @SerializedName(STRIKE)
    private NumericInterval strike;

    /**
     * Getter method for the strike numeric interval.
     *
     * Will find instruments whose strike price falls in an interval.
     *
     * Value should be an Array interval of the form [a, b] where a, b are Numbers or null. At least one entry must be a
     * Number. When both are Numbers, it is required that a <= b.
     *
     * Also, [a, null] is equivalent to the interval [a, infinity) and [null, b] is equivalent to (-infinity, b].
     *
     * @since 2.0.3.1-RELEASE
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public NumericInterval getStrike() {
        return strike;
    }

    /**
     * Setter method for the strike numeric interval.
     *
     * Will find instruments whose strike price falls in an interval.
     *
     * Value should be an Array interval of the form [a, b] where a, b are Numbers or null. At least one entry must be a
     * Number. When both are Numbers, it is required that a <= b.
     *
     * Also, [a, null] is equivalent to the interval [a, infinity) and [null, b] is equivalent to (-infinity, b].
     *
     * @since 2.0.3.1-RELEASE
     *
     * @param strike The strike interval.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public void setStrike(@Changeable(STRIKE) NumericInterval strike) {
        this.strike = strike;
    }

    /**
     * Method sets the strike with the specified {@link DateInterval}.
     *
     * Will find instruments whose strike price falls in an interval.
     *
     * Value should be an Array interval of the form [a, b] where a, b are Numbers or null. At least one entry must be a
     * Number. When both are Numbers, it is required that a <= b.
     *
     * Also, [a, null] is equivalent to the interval [a, infinity) and [null, b] is equivalent to (-infinity, b].
     *
     * @since 2.0.3.1-RELEASE
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withStrike(NumericInterval strike) {

        if (strike == null)
            throw new InvalidMappingEntryConfigurationException("The strike cannot be null.");

        setStrike (
            strike.validate()
        );

        return this;
    }

    /**
     * Method sets the strike with the specified aValue.
     *
     * Will find instruments whose strike price falls in an interval.
     *
     * Value should be an Array interval of the form [a, b] where a, b are Numbers or null. At least one entry must be a
     * Number. When both are Numbers, it is required that a <= b.
     *
     * Also, [a, null] is equivalent to the interval [a, infinity) and [null, b] is equivalent to (-infinity, b].
     *
     * @since 2.0.3.1-RELEASE
     *
     * @param aValue The a value.
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withStrike(Number aValue) {
        return withStrike (new NumericInterval(aValue));
    }

    /**
     * Method sets the strike with the specified aValue.
     *
     * Will find instruments whose strike price falls in an interval.
     *
     * Value should be an Array interval of the form [a, b] where a, b are Numbers or null. At least one entry must be a
     * Number. When both are Numbers, it is required that a <= b.
     *
     * Also, [a, null] is equivalent to the interval [a, infinity) and [null, b] is equivalent to (-infinity, b].
     *
     * @since 2.0.3.1-RELEASE
     *
     * @param aValue The a value.
     *
     * @param bValue The b value.
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withStrike(Number aValue, Number bValue) {
        return withStrike (new NumericInterval(aValue, bValue));
    }

    private static final String CONTRACT_SIZE = "contractSize";

    /**
     * @since 2.0.3.1-RELEASE
     */
    @Expose
    @SerializedName(CONTRACT_SIZE)
    private NumericInterval contractSize;

    /**
     * Getter method for the contract size.
     */
    public NumericInterval getContractSize() {
        return contractSize;
    }

    /**
     * Setter method for the contract size.
     *
     * Will find instruments whose contract size falls in an interval.
     *
     * Value should be an Array interval of the form [a, b] where a, b are Numbers or null.
     *
     * At least one entry must be a Number. When both are Numbers, it is required that a <= b. Also, [a, null] is
     * equivalent to the interval [a, infinity) and [null, b] is equivalent to (-infinity, b].
     *
     * @param contractSize The contract size interval.
     *
     * @since 2.0.3.1-RELEASE
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public void setContractSize(@Changeable(CONTRACT_SIZE) NumericInterval contractSize) {
        this.contractSize = contractSize;
    }

    /**
     * Setter method for the contract size.
     *
     * Will find instruments whose contract size falls in an interval.
     *
     * Value should be an Array interval of the form [a, b] where a, b are Numbers or null.
     *
     * At least one entry must be a Number. When both are Numbers, it is required that a <= b. Also, [a, null] is
     * equivalent to the interval [a, infinity) and [null, b] is equivalent to (-infinity, b].
     *
     * @param contractSize The contract size interval.
     *
     * @since 2.0.3.1-RELEASE
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withContractSize(NumericInterval contractSize) {

        if (contractSize == null)
            throw new InvalidMappingEntryConfigurationException("The contractSize cannot be null.");

        setContractSize (
            contractSize.validate()
        );

        return this;
    }

    /**
     * Setter method for the contract size.
     *
     * Will find instruments whose contract size falls in an interval.
     *
     * Value should be an Array interval of the form [a, b] where a, b are Numbers or null.
     *
     * At least one entry must be a Number. When both are Numbers, it is required that a <= b. Also, [a, null] is
     * equivalent to the interval [a, infinity) and [null, b] is equivalent to (-infinity, b].
     *
     * @param aValue The contract size a value (the bValue is assumed to be null).
     *
     * @since 2.0.3.1-RELEASE
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withContractSize(Number aValue) {
        return withContractSize (new NumericInterval(aValue));
    }

    /**
     * Setter method for the contract size.
     *
     * Will find instruments whose contract size falls in an interval.
     *
     * Value should be an Array interval of the form [a, b] where a, b are Numbers or null.
     *
     * At least one entry must be a Number. When both are Numbers, it is required that a <= b. Also, [a, null] is
     * equivalent to the interval [a, infinity) and [null, b] is equivalent to (-infinity, b].
     *
     * @param aValue The contract size a value.
     * @param bValue The contract size b value.
     *
     * @since 2.0.3.1-RELEASE
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withContractSize(Number aValue, Number bValue) {
        return withContractSize (new NumericInterval(aValue, bValue));
    }

    private static final String COUPON = "coupon";

    /**
     * @since 2.0.3.1-RELEASE
     */
    @Expose
    @SerializedName(COUPON)
    private NumericInterval coupon;

    public NumericInterval getCoupon() {
        return coupon;
    }

    public void setCoupon(@Changeable(COUPON) NumericInterval coupon) {
        this.coupon = coupon;
    }

    /**
     * Setter method for the coupon.
     *
     * Will find instruments whose coupon falls in an interval.
     *
     * Value should be an Array interval of the form [a, b] where a, b are Numbers or null.
     *
     * At least one entry must be a Number. When both are Numbers, it is required that a <= b. Also, [a, null] is
     * equivalent to the interval [a, infinity) and [null, b] is equivalent to (-infinity, b].
     *
     * @param coupon The coupon interval.
     *
     * @since 2.0.3.1-RELEASE
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withCoupon(NumericInterval coupon) {

        if (coupon == null)
            throw new InvalidMappingEntryConfigurationException("The coupon cannot be null.");

        setCoupon (
            coupon.validate()
        );

        return this;
    }

    /**
     * Setter method for the coupon.
     *
     * Will find instruments whose coupon falls in an interval.
     *
     * Value should be an Array interval of the form [a, b] where a, b are Numbers or null.
     *
     * At least one entry must be a Number. When both are Numbers, it is required that a <= b. Also, [a, null] is
     * equivalent to the interval [a, infinity) and [null, b] is equivalent to (-infinity, b].
     *
     * @param aValue The coupon a value (the bValue is assumed to be null).
     *
     * @since 2.0.3.1-RELEASE
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withCoupon(Number aValue) {
        return withCoupon (new NumericInterval(aValue));
    }

    /**
     * Setter method for the coupon.
     *
     * Will find instruments whose coupon falls in an interval.
     *
     * Value should be an Array interval of the form [a, b] where a, b are Numbers or null.
     *
     * At least one entry must be a Number. When both are Numbers, it is required that a <= b. Also, [a, null] is
     * equivalent to the interval [a, infinity) and [null, b] is equivalent to (-infinity, b].
     *
     * @param aValue The coupon a value.
     * @param bValue The coupon b value.
     *
     * @since 2.0.3.1-RELEASE
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withCoupon(Number aValue, Number bValue) {
        return withCoupon (new NumericInterval(aValue, bValue));
    }

    private static final String EXPIRATION = "expiration";

    /**
     * When securityType2 is Option or Warrant.
     *
     * Will find instruments whose expiration date falls in an interval. Value should be an Array interval of the
     * form [a, b] where a, b are date Strings or null.
     *
     * Date strings must be of the form YYYY-MM-DD.
     * 
     * At least one entry must be a date String.
     * 
     * When both are date Strings, it is required that a and b are no more than one year apart. Also, [a, null] is
     * equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
     *
     * @since 2.0.3.1-RELEASE
     */
    @Expose
    @SerializedName(EXPIRATION)
    private DateInterval expiration;

    /**
     * Getter method for the expiration.
     *
     
     *
     * @return An instance of DateInterval or null if none has been set.
     */
    public DateInterval getExpiration() {
        return expiration;
    }

    /**
     * Setter method for the expiration.
     */
    public void setExpiration(@Changeable(EXPIRATION) DateInterval expiration) {
        this.expiration = expiration;
    }

    /**
     * Method sets the expiration with the specified {@link DateInterval}.
     *
     * When securityType2 is Option or Warrant.
     *
     * Will find instruments whose expiration date falls in an interval. Value should be an Array interval of the form
     * [a, b] where a, b are date Strings or null.
     *
     * Date strings must be of the form YYYY-MM-DD.
     *
     * At least one entry must be a date String.
     *
     * When both are date Strings, it is required that a and b are no more than one year apart.
     *
     * Also, [a, null] is equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
     *
     * Note that this method will call the {@link IntervalSpecification#validate()} method.
     *
     * @since 2.0.3.1-RELEASE
     *
     * @param expiration The expiration which cannot be null.
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withExpiration(DateInterval expiration) {

        if (expiration == null)
            throw new NullPointerException ("The expiration cannot be null -- if it must be null either call the " +
                "setter method or simply don't call this method with a null reference.");

        setExpiration(
            expiration.validate()
        );

        return this;
    }

    /**
     * Method sets the expiration with the specified aValue and asumes the bValue is null.
     *
     * When securityType2 is Option or Warrant.
     *
     * Will find instruments whose expiration date falls in an interval. Value should be an Array interval of the form
     * [a, b] where a, b are date Strings or null.
     *
     * Date strings must be of the form YYYY-MM-DD.
     *
     * At least one entry must be a date String.
     *
     * When both are date Strings, it is required that a and b are no more than one year apart.
     *
     * Also, [a, null] is equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
     *
     * Note that this method will call the {@link IntervalSpecification#validate()} method.
     *
     * @since 2.0.3.1-RELEASE
     *
     * @param aValue The a date.
     *
     * @param bValue The b date.
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withExpiration(Date aValue, Date bValue) {
        return withExpiration (new DateInterval (aValue, bValue));
    }

    /**
     * Method sets the expiration with the specified aValue and asumes the bValue is null.
     *
     * When securityType2 is Option or Warrant.
     *
     * Will find instruments whose expiration date falls in an interval. Value should be an Array interval of the form
     * [a, b] where a, b are date Strings or null.
     *
     * Date strings must be of the form YYYY-MM-DD.
     *
     * At least one entry must be a date String.
     *
     * When both are date Strings, it is required that a and b are no more than one year apart.
     *
     * Also, [a, null] is equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
     *
     * Note that this method will call the {@link IntervalSpecification#validate()} method.
     *
     * @since 2.0.3.1-RELEASE
     *
     * @param aValue The a date.
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withExpiration(Date aValue) {
        return withExpiration (new DateInterval (aValue));
    }

    /**
     * Method sets the expiration with the specified aValue and asumes the bValue is null.
     *
     * When securityType2 is Option or Warrant.
     *
     * Will find instruments whose expiration date falls in an interval. Value should be an Array interval of the form
     * [a, b] where a, b are date Strings or null.
     *
     * Date strings must be of the form YYYY-MM-DD.
     *
     * At least one entry must be a date String.
     *
     * When both are date Strings, it is required that a and b are no more than one year apart.
     *
     * Also, [a, null] is equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
     *
     * Note that this method will call the {@link IntervalSpecification#validate()} method.
     *
     * @since 2.0.3.1-RELEASE
     *
     * @param aValue The a date string in the form of YYYY-MM-DD, for example: 2019-09-01.
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withExpiration(String aValue) {
        return withExpiration (new DateInterval (aValue));
    }

    /**
     * Method sets the expiration with the specified aValue and bValue date strings.
     *
     * When securityType2 is Option or Warrant.
     *
     * Will find instruments whose expiration date falls in an interval. Value should be an Array interval of the form
     * [a, b] where a, b are date Strings or null.
     *
     * Date strings must be of the form YYYY-MM-DD.
     *
     * At least one entry must be a date String.
     *
     * When both are date Strings, it is required that a and b are no more than one year apart.
     *
     * Also, [a, null] is equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
     *
     * Note that this method will call the {@link IntervalSpecification#validate()} method.
     *
     * @since 2.0.3.1-RELEASE
     *
     * @param aValue The a date string in the form of YYYY-MM-DD, for example: 2019-09-01.
     *
     * @param bValue The b date string in the form of YYYY-MM-DD, for example: 2019-09-01.
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withExpiration(String aValue, String bValue) {
        return withExpiration (new DateInterval (aValue, bValue));
    }

    private static final String MATURITY = "maturity";

    /**
     * When securityType2 is Pool.
     *
     * Will find instruments whose maturity date falls in an interval. Value should be an Array interval of the
     * form [a, b] where a, b are date Strings or null. Date strings must be of the form YYYY-MM-DD.
     *
     * At least one entry must be a date String.
     *
     * When both are date Strings, it is required that a and b are no more than one year apart. Also, [a, null] is
     * equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
     *
     * @since 2.0.3.1-RELEASE
     */
    @Expose
    @SerializedName(MATURITY)
    private DateInterval maturity;

    /**
     * Getter method for the maturity.
     *
     * When securityType2 is Pool.
     *
     * Will find instruments whose maturity date falls in an interval. Value should be an Array interval of the
     * form [a, b] where a, b are date Strings or null. Date strings must be of the form YYYY-MM-DD.
     *
     * At least one entry must be a date String.
     *
     * When both are date Strings, it is required that a and b are no more than one year apart. Also, [a, null] is
     * equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
     *
     * @since 2.0.3.1-RELEASE
     *
     * @return An instance of DateInterval or null if none has been set.
     */
    public DateInterval getMaturity() {
        return maturity;
    }

    /**
     * Setter method for the maturity.
     *
     * When securityType2 is Pool.
     *
     * Will find instruments whose maturity date falls in an interval. Value should be an Array interval of the
     * form [a, b] where a, b are date Strings or null. Date strings must be of the form YYYY-MM-DD.
     *
     * At least one entry must be a date String.
     *
     * When both are date Strings, it is required that a and b are no more than one year apart. Also, [a, null] is
     * equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
     *
     * @since 2.0.3.1-RELEASE
     *
     * @param maturity The maturity.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public void setMaturity(@Changeable(MATURITY) DateInterval maturity) {
        this.maturity = maturity;
    }

    /**
     * Method sets the maturity with the specified {@link DateInterval}.
     *
     * When securityType2 is Pool.
     *
     * Will find instruments whose maturity date falls in an interval. Value should be an Array interval of the
     * form [a, b] where a, b are date Strings or null. Date strings must be of the form YYYY-MM-DD.
     *
     * At least one entry must be a date String.
     *
     * When both are date Strings, it is required that a and b are no more than one year apart. Also, [a, null] is
     * equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
     *
     * Note that this method will call the {@link IntervalSpecification#validate()} method.
     *
     * @since 2.0.3.1-RELEASE
     *
     * @param maturity The maturity which cannot be null.
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws NullPointerException When the maturity is null.
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withMaturity(DateInterval maturity) {

        if (maturity == null)
            throw new NullPointerException ("The maturity cannot be null -- if it must be null either call the " +
                "setter method or simply don't call this method with a null reference.");

        setMaturity(
            maturity.validate()
        );

        return this;
    }

    /**
     * Method sets the maturity using the specified aValue and bValue dates.
     *
     * When securityType2 is Pool.
     *
     * Will find instruments whose maturity date falls in an interval. Value should be an Array interval of the
     * form [a, b] where a, b are date Strings or null. Date strings must be of the form YYYY-MM-DD.
     *
     * At least one entry must be a date String.
     *
     * When both are date Strings, it is required that a and b are no more than one year apart. Also, [a, null] is
     * equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
     *
     * Note that this method will call the {@link IntervalSpecification#validate()} method.
     *
     * @since 2.0.3.1-RELEASE
     *
     * @param aValue A date.
     * 
     * @param bValue B date.
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withMaturity(Date aValue, Date bValue) {
        return withMaturity (new DateInterval (aValue, bValue));
    }

    /**
     * Method sets the maturity with the specified aValue date and assumes the bValue date is null.
     *
     * When securityType2 is Pool.
     *
     * Will find instruments whose maturity date falls in an interval. Value should be an Array interval of the
     * form [a, b] where a, b are date Strings or null. Date strings must be of the form YYYY-MM-DD.
     *
     * At least one entry must be a date String.
     *
     * When both are date Strings, it is required that a and b are no more than one year apart. Also, [a, null] is
     * equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
     *
     * Note that this method will call the {@link IntervalSpecification#validate()} method.
     *
     * @since 2.0.3.1-RELEASE
     *
     * @param aValue The a date.
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withMaturity(Date aValue) {
        return withMaturity (new DateInterval (aValue));
    }

    /**
     * Method sets the maturity with the specified aValue date string and assumes the bValue is null.
     *
     * When securityType2 is Pool.
     *
     * Will find instruments whose maturity date falls in an interval. Value should be an Array interval of the
     * form [a, b] where a, b are date Strings or null. Date strings must be of the form YYYY-MM-DD.
     *
     * At least one entry must be a date String.
     *
     * When both are date Strings, it is required that a and b are no more than one year apart. Also, [a, null] is
     * equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
     *
     * Note that this method will call the {@link IntervalSpecification#validate()} method.
     *
     * @since 2.0.3.1-RELEASE
     *
     * @param aValue The a date in the form of YYYY-MM-DD, for example: 2019-09-01.
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withMaturity(String aValue) {
        return withMaturity (new DateInterval (aValue));
    }

    /**
     * Method sets the maturity with the specified aValue and bValue date strings.
     *
     * When securityType2 is Pool.
     *
     * Will find instruments whose maturity date falls in an interval. Value should be an Array interval of the
     * form [a, b] where a, b are date Strings or null. Date strings must be of the form YYYY-MM-DD.
     *
     * At least one entry must be a date String.
     *
     * When both are date Strings, it is required that a and b are no more than one year apart. Also, [a, null] is
     * equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
     *
     * Note that this method will call the {@link IntervalSpecification#validate()} method.
     *
     * @since 2.0.3.1-RELEASE
     *
     * @param aValue The a date string in the form of YYYY-MM-DD, for example: 2019-09-01.
     *
     * @param bValue The b date string in the form of YYYY-MM-DD, for example: 2019-09-01.
     *
     * @return A reference to this which is used to facilitate method chaining.
     *
     * @throws InvalidIntervalRuntimeException When validation fails.
     *
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withMaturity(String aValue, String bValue) {
        return withMaturity (new DateInterval (aValue, bValue));
    }

    // -----

    @Transient
    private transient final RequestBody requestBody;

    public MappingEntry(RequestBody requestBody) {
        this.requestBody = requestBody;
    }

    /**
     * Used strictly to facilitate method chaining -- returns an instance of the {@link RequestBody}.
     */
    public RequestBody done () {
        return requestBody;
    }

    /**
     * Type of third party identifier. See Supported Identifiers for all supported 3rd party identifier types.
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public String getIdType() {
        return idType;
    }

    public static final String ID_TYPE = "idType";

    /**
     * Type of third party identifier. See Supported Identifiers for all supported 3rd party identifier types.
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public void setIdType(@Changeable(ID_TYPE) String idType) {
        this.idType = idType;
    }

    /**
     * Type of third party identifier. See Supported Identifiers for all supported 3rd party identifier types.
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public void setIdType(IdType idType) {
        setIdType(idType.toString());
    }

    /**
     * @see #withIdType(String)
     * @see #withIdValue(String)
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withIdTypeAndValue (String idType, String idValue) {
        return withIdType (idType).withIdValue(idValue);
    }

    /**
     * @see #withIdType(String)
     * @see #withIdValue(String)
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withIdTypeAndValue (IdType idType, String idValue) {
        return withIdType (idType.toString()).withIdValue(idValue);
    }

    /**
     * @see #withIdType(String)
     * @see #withIdValue(Number)
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withIdTypeAndValue (String idType, Number idValue) {
        return withIdType (idType).withIdValue(idValue);
    }

    /**
     * @see #withIdType(String)
     * @see #withIdValue(Number)
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     *
     * @deprecated The idValue should always be a string.
     */
    public MappingEntry withIdTypeAndValue (IdType idType, Number idValue) {
        return withIdType (idType.toString()).withIdValue(idValue);
    }

    /**
     * Type of third party identifier. See Supported Identifiers for all supported 3rd party identifier types.
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withIdType (String idType) {

        setIdType (idType);

        return this;
    }

    /**
     * Type of third party identifier. See Supported Identifiers for all supported 3rd party identifier types.
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withIdType (IdType idType) {

        setIdType (idType);

        return this;
    }

    /**
     * The value for the represented third party identifier.
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public String getIdValue() {
        return idValue;
    }

    public static final String ID_VALUE = "idValue";

    /**
     * The value for the represented third party identifier.
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public void setIdValue(@Changeable(ID_VALUE) String idValue) {
        this.idValue = idValue;
    }

    /**
     * The value for the represented third party identifier.
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     *
     * @deprecated The idValue should always be a string.
     */
    public void setIdValue(Number idValue) {
        setIdValue(assertNotNull (ID_VALUE, idValue).toString());
    }

    /**
     * The value for the represented third party identifier.
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withIdValue(String idValue) {

        setIdValue (idValue);

        return this;
    }

    /**
     * The value for the represented third party identifier.
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     *
     * @deprecated The idValue should always be a string.
     */
    public MappingEntry withIdValue(Number idValue) {

        setIdValue (assertNotNull (ID_VALUE, idValue).toString());

        return this;
    }

    /**
     * An optional exchange code if it applies (cannot use with micCode).
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/exchCode">OpenFIGI Exchange Codes</a>
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public String getExchangeCode() {
        return exchangeCode;
    }

    public static final String EXCHANGE_CODE = "exchangeCode";

    /**
     * An optional exchange code if it applies (cannot use with micCode).
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/exchCode">OpenFIGI Exchange Codes</a>
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public void setExchangeCode(@Changeable(EXCHANGE_CODE) String exchangeCode) {

        if (marketIdentificationCode != null && exchangeCode != null)
            throw new InvalidMappingEntryConfigurationException ("The marketIdentificationCode (micCode) must be "
                + "non-null when setting the exchangeCode (marketIdentificationCode: " + marketIdentificationCode
                + ").");

        this.exchangeCode = exchangeCode;
    }

    /**
     * An optional exchange code if it applies (cannot use with micCode).
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/exchCode">OpenFIGI Exchange Codes</a>
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public void setExchangeCode(ExchCode exchangeCode) {
        setExchangeCode(exchangeCode.toString());
    }

    /**
     * Exchange code of the desired instrument(s) (cannot use in conjunction with micCode).
     * <a href="https://openfigi.com/api/enumValues/v3/exchCode">Click here</a> for the current list of exchCode values.
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     * @see <a href="https://openfigi.com/api/enumValues/v3/exchCode">OpenFIGI Exchange Codes</a>
     */
    public MappingEntry withExchangeCode(String exchangeCode) {

        setExchangeCode(exchangeCode);

        return this;
    }

    /**
     * Exchange code of the desired instrument(s) (cannot use in conjunction with micCode).
     * <a href="https://openfigi.com/api/enumValues/v3/exchCode">Click here</a> for the current list of exchCode values.
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     * @see <a href="https://openfigi.com/api/enumValues/v3/exchCode">OpenFIGI Exchange Codes</a>
     */
    public MappingEntry withExchangeCode(ExchCode exchangeCode) {
        return withExchangeCode (exchangeCode.toString());
    }

    /**
     * An optional ISO market identification code(MIC) if it applies(cannot use with exchCode).
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/micCode">ISO market identification code(MIC)</a>
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public String getMarketIdentificationCode() {
        return marketIdentificationCode;
    }

    public static final String MARKET_IDENTIFICATION_CODE = "marketIdentificationCode";

    /**
     * An optional ISO market identification code(MIC) if it applies(cannot use with exchCode).
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/micCode">ISO market identification code(MIC)</a>
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public void setMarketIdentificationCode(@Changeable(MARKET_IDENTIFICATION_CODE) String marketIdentificationCode) {

        if (exchangeCode != null && marketIdentificationCode != null)
            throw new InvalidMappingEntryConfigurationException ("The exchangeCode (exchCode) must be "
                + "non-null when setting the marketIdentificationCode (exchangeCode: " + exchangeCode + ").");

        this.marketIdentificationCode = marketIdentificationCode;
    }

    /**
     * An optional ISO market identification code(MIC) if it applies(cannot use with exchCode).
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/micCode">ISO market identification code(MIC)</a>
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public void setMarketIdentificationCode(MicCode marketIdentificationCode) {
        setMarketIdentificationCode(marketIdentificationCode.toString());
    }

    /**
     * An optional ISO market identification code(MIC) if it applies(cannot use with exchCode).
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/micCode">ISO market identification code(MIC)</a>
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withMarketIdentificationCode(String marketIdentificationCode) {

        setMarketIdentificationCode(marketIdentificationCode);

        return this;
    }

    /**
     * An optional ISO market identification code(MIC) if it applies(cannot use with exchCode).
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/micCode">ISO market identification code(MIC)</a>
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withMarketIdentificationCode(MicCode marketIdentificationCode) {
        return withMarketIdentificationCode (marketIdentificationCode.toString());
    }

    /**
     * An optional currency if it applies.
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/currency">Currency associated to the desired
     *  instrument(s).</a>
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public String getCurrency() {
        return currency;
    }

    public static final String CURRENCY = "currency";

    /**
     * An optional currency if it applies.
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/currency">Currency associated to the desired
     *  instrument(s).</a>
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public void setCurrency(@Changeable(CURRENCY) String currency) {
        this.currency = currency;
    }

    /**
     * An optional currency if it applies.
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/currency">Currency associated to the desired
     *  instrument(s).</a>
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public void setCurrency(Currency currency) {
        setCurrency (currency.toString());
    }

    /**
     * An optional currency if it applies.
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/currency">Currency associated to the desired
     *  instrument(s).</a>
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withCurrency(String currency) {

        setCurrency(currency);

        return this;
    }

    /**
     * An optional currency if it applies.
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/currency">Currency associated to the desired
     *  instrument(s).</a>
     *
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withCurrency(Currency currency) {
        return withCurrency (currency.toString());
    }

    /**
     * An optional market sector description if it applies.
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/marketSecDes">OpenFIGI Market Sector Description</a>
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public String getMarketSectorDescription() {
        return marketSectorDescription;
    }

    public static final String MARKET_SECTOR_DESCRIPTION = "marketSectorDescription";

    /**
     * An optional market sector description if it applies.
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/marketSecDes">OpenFIGI Market Sector Description</a>
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public void setMarketSectorDescription(@Changeable(MARKET_SECTOR_DESCRIPTION) String marketSectorDescription) {
        this.marketSectorDescription = marketSectorDescription;
    }

    /**
     * An optional market sector description if it applies.
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/marketSecDes">OpenFIGI Market Sector Description</a>
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public void setMarketSectorDescription(MarketSecDes marketSectorDescription) {
        setMarketSectorDescription (marketSectorDescription.toString ());
    }

    /**
     * An optional market sector description if it applies.
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/marketSecDes">OpenFIGI Market Sector Description</a>
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withMarketSectorDescription(String marketSectorDescription) {

        setMarketSectorDescription(marketSectorDescription);

        return this;
    }

    /**
     * An optional market sector description if it applies.
     *
     * @see <a href="https://openfigi.com/api/enumValues/v3/marketSecDes">OpenFIGI Market Sector Description</a>
     * @see <a href="https://www.openfigi.com/api">OpenFIGI API</a>
     */
    public MappingEntry withMarketSectorDescription(MarketSecDes marketSectorDescription) {
        return withMarketSectorDescription (marketSectorDescription.toString());
    }

    @Override    
    public String toString() {
        return "MappingEntry [idType=" + idType + ", idValue=" + idValue + ", exchangeCode=" + exchangeCode
            + ", marketIdentificationCode=" + marketIdentificationCode + ", currency=" + currency
            + ", marketSectorDescription=" + marketSectorDescription + ", securityType=" + securityType
            + ", securityType2=" + securityType2 + ", includeUnlistedEquities=" + includeUnlistedEquities
            + ", optionType=" + optionType + ", strike=" + strike + ", contractSize=" + contractSize + ", coupon="
            + coupon + ", expiration=" + expiration + ", maturity=" + maturity + "]";
    }
}