package com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain;

/**
 * The interval is used in the
 * strike : Array(2) / Will find instruments whose strike price falls in an interval. Value should be an Array interval of the form [a, b] where a, b are Numbers or null. At least one entry must be a Number. When both are Numbers, it is required that a <= b. Also, [a, null] is equivalent to the interval [a, infinity) and [null, b] is equivalent to (-infinity, b].
 *
 * contractSize : Array(2) / Will find instruments whose contract size falls in an interval. Value should be an Array interval of the form [a, b] where a, b are Numbers or null. At least one entry must be a Number. When both are Numbers, it is required that a <= b. Also, [a, null] is equivalent to the interval [a, infinity) and [null, b] is equivalent to (-infinity, b].
 * 
 * coupon : Array(2) / Will find instruments whose coupon falls in an interval. Value should be an Array interval of the form [a, b] where a, b are Numbers or null. At least one entry must be a Number. When both are Numbers, it is required that a <= b. Also, [a, null] is equivalent to the interval [a, infinity) and [null, b] is equivalent to (-infinity, b].
 *
 * expiration : Array(2) / When securityType2 is Option or Warrant / Will find instruments whose expiration date falls in an interval. Value should be an Array interval of the form [a, b] where a, b are date Strings or null. Date strings must be of the form YYYY-MM-DD. At least one entry must be a date String. When both are date Strings, it is required that a and b are no more than one year apart. Also, [a, null] is equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
 *
 * maturity : Array(2) / When securityType2 is Pool / Will find instruments whose maturity date falls in an interval. Value should be an Array interval of the form [a, b] where a, b are date Strings or null. Date strings must be of the form YYYY-MM-DD. At least one entry must be a date String. When both are date Strings, it is required that a and b are no more than one year apart. Also, [a, null] is equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
 *
 * https://openfigi.com/api
 * 
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public abstract class AbstractInterval<T> implements IntervalSpecification<T> {

    private T aValue;

    private T bValue;

    public AbstractInterval (T aValue) {
        this (aValue, null);
    }

    public AbstractInterval (T aValue, T bValue) {
        this.aValue = aValue;
        this.bValue = bValue;
    }

    public T getAValue() {
        return aValue;
    }

    public String getAValueAsString () {
        return aValue == null ? null : aValue.toString();
    }

    public void setAValue(T aValue) {
        this.aValue = aValue;
    }

    public T getBValue() {
        return bValue;
    }

    public void setBValue(T bValue) {
        this.bValue = bValue;
    }

    public String getBValueAsString () {
        return bValue == null ? null : bValue.toString();
    }

    public abstract <X extends AbstractInterval<T>> X validate ();

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((aValue == null) ? 0 : aValue.hashCode());
        result = prime * result + ((bValue == null) ? 0 : bValue.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AbstractInterval<?> other = (AbstractInterval<?>) obj;
        if (aValue == null) {
            if (other.aValue != null)
                return false;
        } else if (!aValue.equals(other.aValue))
            return false;
        if (bValue == null) {
            if (other.bValue != null)
                return false;
        } else if (!bValue.equals(other.bValue))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "AbstractInterval [aValue=" + aValue + ", bValue=" + bValue + "]";
    }
}
