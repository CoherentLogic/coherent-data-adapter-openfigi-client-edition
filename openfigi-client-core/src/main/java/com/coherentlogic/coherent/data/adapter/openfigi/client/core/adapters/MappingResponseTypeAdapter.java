package com.coherentlogic.coherent.data.adapter.openfigi.client.core.adapters;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.coherentlogic.coherent.data.adapter.core.exceptions.MethodNotSupportedException;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.MappingResponse;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.MappingValue;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

/**
 * An adapter that converts JSON into an instance of {@link MappingResponse}.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class MappingResponseTypeAdapter extends TypeAdapter<MappingResponse> {

    private static final Logger log = LoggerFactory.getLogger(MappingResponseTypeAdapter.class);

    public static final String BEAN_NAME = "mappingResponseTypeAdapter";

    private final GsonBuilder gsonBuilder;

    @Autowired
    private ApplicationContext applicationContext;

    public MappingResponseTypeAdapter(
        GsonBuilder gsonBuilder
    ) {
        this.gsonBuilder = gsonBuilder;
    }

    @Override
    public MappingResponse read(JsonReader reader) throws IOException {

        log.info("read: method begins; reader: " + reader);

        MappingResponse result = applicationContext.getBean(MappingResponse.class);

        //Gson gson = gsonBuilder.create();

        reader.beginObject ();

        String values = reader.nextName();

        if (MappingResponse.VALUES.equals(values)) {

            reader.beginArray();

            while (reader.hasNext()) {

                String nextValue = reader.nextString();

                result.addMappingValue(new MappingValue ()).setValue(nextValue);
            }

            reader.endArray();

        } else throw new IllegalStateException (
            "Expected 'values' so it's possible the response data model has changed.");

        reader.endObject ();

        return result;
    }

//    /**
//     * @todo Combine the addMappingResponse and addErrors methods.
//     */
//    List<MappingResponseEntry> addMappingResponse (Gson gson, JsonElement dataElement) {
//
//        List<MappingResponseEntry> result = new ArrayList<MappingResponseEntry> ();
//
//        if (dataElement != null && !dataElement.isJsonNull()) {
//
//            JsonArray dataArray = dataElement.getAsJsonArray();
//
//            log.info("dataArray: " + dataArray);
//
//            Iterator<JsonElement> iterator = dataArray.iterator();
//
//            while (iterator.hasNext()) {
//
//                JsonElement nextElement = iterator.next();
//
//                JsonObject nextObject = nextElement.getAsJsonObject(); // data
//
//                MappingResponseEntry nextEntry = gson.fromJson(nextObject, MappingResponse.class);
//
//                result.add(nextEntry);
//
//                log.info("nextEntry: " + nextEntry);
//            }
//        }
//        return result;
//    }
//
//    /**
//     * @todo Combine the addMappingResponse and addErrors methods.
//     */
//    List<ErrorEntry> addErrors (Gson gson, JsonElement errorElement) {
//
//        List<ErrorEntry> result = new ArrayList<ErrorEntry> ();
//
//        if (errorElement !=null && !errorElement.isJsonNull()) {
//
//            if (errorElement.isJsonArray()) {
//
//                JsonArray errorArray = errorElement.getAsJsonArray();
//
//                Iterator<JsonElement> iterator = errorArray.iterator();
//
//                while (iterator.hasNext()) {
//
//                    JsonElement nextElement = iterator.next();
//
//                    JsonObject nextObject = nextElement.getAsJsonObject(); // error
//
//                    ErrorEntry nextEntry = gson.fromJson(nextObject, ErrorEntry.class);
//
//                    result.add(nextEntry);
//
//                    log.info("nextEntry: " + nextEntry);
//                }
//            } else {
//
//                String errorText = errorElement.getAsString();
//
//                log.info("errorText: " + errorText);
//
//                ErrorEntry nextEntry = applicationContext.getBean(ErrorEntry.class);
//
//                nextEntry.setError(errorText);
//
//                result.add(nextEntry);
//            }
//        }
//        return result;
//    }

    @Override
    public void write(JsonWriter arg0, MappingResponse arg1) throws IOException {
        throw new MethodNotSupportedException ("The write method is not supported.");
    }
}
