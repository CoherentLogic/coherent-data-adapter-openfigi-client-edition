package com.coherentlogic.coherent.data.adapter.openfigi.client.core.serializers;

import java.lang.reflect.Type;

import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.NumericInterval;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Validates and converts the interval into a JSON array.
 *
 * @see <a href="https://www.openfigi.com/api">The OpenFIGI API</a>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class NumericIntervalSerializer implements JsonSerializer<NumericInterval> {

    @Override
    public JsonElement serialize(NumericInterval numericInterval, Type type, JsonSerializationContext context) {

        numericInterval.validate();

        JsonArray result = new JsonArray();

        Number aValue = numericInterval.getAValue();
        Number bValue = numericInterval.getBValue();

        result.add (aValue == null ? null : new JsonPrimitive (aValue));
        result.add (bValue == null ? null : new JsonPrimitive (bValue));

        return result;
    }
}
