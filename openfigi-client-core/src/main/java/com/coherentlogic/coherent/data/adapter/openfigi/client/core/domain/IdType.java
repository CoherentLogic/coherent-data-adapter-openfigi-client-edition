package com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain;


/**
 * An enumeration of available id type values.
 *
 * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public enum IdType {

    /**
     * ISIN - International Securities Identification Number.
     *
     * Example:
     *
     * [{"idType":"ID_ISIN","idValue":"XX1234567890"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    ID_ISIN,
    /**
     * Unique Bloomberg Identifier - A legacy, internal Bloomberg identifier.
     *
     * Example:
     *
     * [{"idType":"ID_BB_UNIQUE","idValue":"EQ0010080100001000"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    ID_BB_UNIQUE,
    /**
     * Sedol Number - Stock Exchange Daily Official List.
     *
     * Example:
     *
     * [{"idType":"ID_SEDOL","idValue":"1234567"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    ID_SEDOL,
    /**
     * Common Code - A nine digit identification number.
     *
     * Example:
     *
     * [{"idType":"ID_COMMON","idValue":"123456789"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    ID_COMMON,
    /**
     * Wertpapierkennnummer/WKN - German securities identification code.
     *
     * Example:
     *
     * [{"idType":"ID_WERTPAPIER","idValue":"123456"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    ID_WERTPAPIER,
    /**
     * CUSIP - Committee on Uniform Securities Identification Procedures.
     *
     * Example:
     *
     * [{"idType":"ID_CUSIP","idValue":"123456789"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    ID_CUSIP,
    /**
     * CINS - CUSIP International Numbering System.
     *
     * Example:
     *
     * [{"idType":"ID_CINS","idValue":"123456789"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    ID_CINS,
    /**
     * ID BB - A legacy internal Bloomberg identifier.
     *
     * Example:
     *
     * [{"idType":"ID_BB","idValue":"123456789"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    ID_BB,
    /**
     * Italian Identifier Number - The Italian Identification number consisting of five or six digits.
     *
     * Example:
     *
     * [{"idType":"ID_ITALY","idValue":"123456"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    ID_ITALY,
    /**
     * Local Exchange Security Symbol - Local exchange security symbol.
     *
     * Example:
     *
     * [{"idType":"ID_EXCH_SYMBOL","idValue":"IBM"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    ID_EXCH_SYMBOL,
    /**
     * Full Exchange Symbol - Contains the exchange symbol for futures, options, indices inclusive of base symbol and
     * other security elements.
     *
     * Example:
     *
     * [{"idType":"ID_FULL_EXCHANGE_SYMBOL", "idValue":"IBM  A1819C150000"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    ID_FULL_EXCHANGE_SYMBOL,
    /**
     * Composite Financial Instrument Global Identifier - The Composite Financial Instrument Global Identifier (FIGI)
     * enables users to link multiple FIGIs at the trading venue level within the same country or market in order to
     * obtain an aggregated view for an instrument within that country or market.
     *
     * Example:
     *
     * [{"idType":"COMPOSITE_ID_BB_GLOBAL", "idValue":"BBG000BLNNH6"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    COMPOSITE_ID_BB_GLOBAL,
    /**
     * Share Class Financial Instrument Global Identifier - A Share Class level Financial Instrument Global Identifier
     * is assigned to an instrument that is traded in more than one country. This enables users to link multiple
     * composite FIGIs for the same instrument in order to obtain an aggregated view for that instrument across all
     * countries (globally).
     *
     * Example:
     *
     * [{"idType":"ID_BB_GLOBAL_SHARE_CLASS_LEVEL", "idValue":"BBG001S5S399"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    ID_BB_GLOBAL_SHARE_CLASS_LEVEL,
    /**
     * Financial Instrument Global Identifier (FIGI) - An identifier that is assigned to instruments of all asset
     * classes and is unique to an individual instrument. Once issued, the FIGI assigned to an instrument will not
     * change.
     *
     * Example:
     *
     * [{"idType":"ID_BB_GLOBAL", "idValue":"BBG0000362Y4"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    ID_BB_GLOBAL,
    /**
     * Security ID Number Description - Descriptor for a financial instrument. Similar to the ticker field, but will
     * provide additional metadata data.
     *
     * Example:
     *
     * [{"idType":"ID_BB_SEC_NUM_DES", "idValue":"IBM 7 10/30/25"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    ID_BB_SEC_NUM_DES,
    /**
     * Ticker - Ticker is a specific identifier for a financial instrument that reflects common usage.
     *
     * Example:
     *
     * [{"idType":"TICKER", "idValue":"IBM"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    TICKER,
    /**
     * An indistinct identifier which may be linked to multiple instruments. May need to be combined with other values
     * to identify a unique instrument.
     *
     * Example:
     *
     * [{"idType":"BASE_TICKER","idValue":"IBM"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    BASE_TICKER,
    /**
     * CUSIP (8 Characters Only) - Committee on Uniform Securities Identification Procedures.
     *
     * Example:
     *
     * [{"idType":"ID_CUSIP_8_CHR","idValue":"12345678"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    ID_CUSIP_8_CHR,
    /**
     * OCC Symbol - A twenty-one character option symbol standardized by the Options Clearing Corporation (OCC) to
     * identify a U.S. option.
     *
     * Example:
     *
     * [{"idType":"OCC_SYMBOL","idValue":"IBM 170630C00120000"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    OCC_SYMBOL,
    /**
     * Unique Identifier for Future Option - Bloomberg unique ticker with logic for index, currency, single stock
     * futures, commodities and commodity options.
     *
     * Example:
     *
     * [{"idType":"UNIQUE_ID_FUT_OPT","idValue":"IBMG=Z7 SA Equity"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    UNIQUE_ID_FUT_OPT,
    /**
     * OPRA Symbol - Option symbol standardized by the Options Price Reporting Authority (OPRA) to identify a U.S.
     * option.
     *
     * Example:
     *
     * [{"idType":"OPRA_SYMBOL","idValue":"IBM F3017C120000"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    OPRA_SYMBOL,
    /**
     * Trading System Identifier - Unique identifier for the instrument as used on the source trading system.
     *
     * Example:
     *
     * [{"idType":"TRADING_SYSTEM_IDENTIFIER","idValue":"FZH18 IBMG"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    TRADING_SYSTEM_IDENTIFIER,
    /**
     * An exchange venue specific code to identify fixed income instruments primarily traded in Asia.
     *
     * Example:
     *
     * [{"idType":"ID_SHORT_CODE","idValue":"120521.SH"}]
     *
     * @see <a href="https://openfigi.com/api#v3-idType-values">OpenFIGI API V3 idType values</a>
     */
    ID_SHORT_CODE;

}
