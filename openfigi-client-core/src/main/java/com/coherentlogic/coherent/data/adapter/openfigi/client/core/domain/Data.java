package com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * Holds the results of a call to the OpenFIGI web services and contains entries of type {@link DataEntry} or
 * {@link ErrorEntry}.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class Data extends SerializableBean {

    private static final long serialVersionUID = -5943967236255811134L;

    public static final String OPEN_FIGI_DATA = "openFIGIData";

    private final List<List<? extends SerializableBean>> entries;

    public Data() {
        this (new ArrayList<List<? extends SerializableBean>> ());
    }

    public Data(List<List<? extends SerializableBean>> dataEntries) {
        this.entries = dataEntries;
    }

    public List<List<? extends SerializableBean>> getEntries() {
        return entries;
    }

    public void forEach (Consumer<List<? extends SerializableBean>> consumer) {

        if (entries == null)
            throw new NullPointerException("The entries property is null.");
        else
            entries.forEach(consumer);
    }

    /**
     * @todo Move this to the EDA.
     *
     * @param visitor
     */
    public void accept(Consumer<SerializableBean> visitor) {
        accept(Arrays.asList(visitor));
    }

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {

        super.accept(visitors);

        forEach(
            entry -> {
                entry.forEach(
                    consumer -> {
                        consumer.accept(visitors);
                    }
                );
            }
        );
    }

    @Override
    public String toString() {
        return "Data [entries=" + entries + "]";
    }
}
