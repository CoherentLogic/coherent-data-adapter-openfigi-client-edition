package com.coherentlogic.coherent.data.adapter.openfigi.client.core.extractors;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;

import com.coherentlogic.coherent.data.adapter.openfigi.client.core.exceptions.BadRequestException;

public class AbstractResponseExtractor {

    private static final Logger log = LoggerFactory.getLogger(AbstractResponseExtractor.class);

    public String extractBody (ClientHttpResponse response) throws IOException {

        if (HttpStatus.BAD_REQUEST.equals(response.getStatusCode()))
            throw new BadRequestException ("Details include: rawStatusCode: "
                + response.getStatusCode().toString() + ", statusText: " + response.getStatusText());

        InputStream in = response.getBody();

        String json = IOUtils.toString(in, StandardCharsets.UTF_8);

        log.debug("json: " + json);

        return json;
    }
}
