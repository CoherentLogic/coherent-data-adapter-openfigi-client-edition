package com.coherentlogic.coherent.data.adapter.openfigi.client.core.builders;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;

import javax.ws.rs.core.UriBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;

import com.coherentlogic.coherent.data.adapter.core.adapters.InReturnAdapterSpecification;
import com.coherentlogic.coherent.data.adapter.core.builders.ApiKeyRequiredSpecification;
import com.coherentlogic.coherent.data.adapter.core.builders.rest.AbstractRESTQueryBuilder;
import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.adapter.core.util.WelcomeMessage;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.adapters.RequestBodyAdapter;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.Data;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.Key;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.MappingResponse;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.RequestBody;
import com.coherentlogic.coherent.data.adapter.core.cache.CacheKeyAwareSpecification;

/**
 * Builder for querying the OpenFIGI.com web services -- for example:
 * <pre>
 * Data data = queryBuilder
 *     .withApiKey(API_KEY)
 *     .getRequestBody()
 *         .withIsin("US4592001014")
 *         .withWertpapier("851399")
 *     .done()
 * .doGet();
 * </pre>
 * Below is an equivalent example to the one above, but with the IdType and IdValue set directly on the mapping entry:
 * <pre>
 * Data data = queryBuilder
 *     .withApiKey(API_KEY)
 *     .getRequestBody()
 *         .newMappingEntry()
 *             .withIdType("ID_ISIN")
 *             .withIdValue("US4592001014")
 *         .done()
 *         .newMappingEntry()
 *             .withIdType("ID_WERTPAPIER")
 *             .withIdValue("851399")
 *         .done()
 *     .done()
 * .doGet();
 * </pre>
 *
 * @see <a href="https://bitbucket.org/CoherentLogic/coherent-data-adapter-openfigi-client-edition/src/master/openfigi-client-example/src/main/groovy/SimpleExample.groovy">
 *  The SimpleExample.groovy script has a complete and working example.</a>
 *
 * @see <a href="https://www.openfigi.com/api">The OpenFIGI API</a>
 * @see <a href="https://openfigi.com/assets/local/figi-allocation-rules.pdf">FIGI Allocation Rules</a>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class QueryBuilder extends AbstractRESTQueryBuilder<RequestKey>
    implements ApiKeyRequiredSpecification, CacheKeyAwareSpecification<RequestKey> {

    public static final String DEFAULT_URI = "https://api.openfigi.com/v3/mapping",
        X_OPENFIGI_APIKEY = "X-OPENFIGI-APIKEY";

    private static final Logger log = LoggerFactory.getLogger(QueryBuilder.class);

    private final RequestBody requestBody;

    private final Map<String, String> headers;

    private final InReturnAdapterSpecification<RequestBody, String> requestBodyAdapter;

    private final ResponseExtractor<Data> dataExtractor;

    private final ResponseExtractor<MappingResponse> mappingResponseExtractor;

    static final String[] WELCOME_MESSAGE = {
        " OpenFIGI Client Edition Version 3.0.3-RELEASE"
    };

    static {

        WelcomeMessage welcomeMessage = new WelcomeMessage();

        for (String next : WELCOME_MESSAGE) {
            welcomeMessage.addText(next);
        }

        welcomeMessage.display();
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        ResponseExtractor<Data> dataExtractor,
        ResponseExtractor<MappingResponse> mappingResponseExtractor
    ) {
        this(restTemplate, DEFAULT_URI, new RequestBodyAdapter (), dataExtractor, mappingResponseExtractor);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        String uri,
        InReturnAdapterSpecification<RequestBody, String> requestBodyAdapter,
        ResponseExtractor<Data> dataExtractor,
        ResponseExtractor<MappingResponse> mappingResponseExtractor
    ) {
        super(restTemplate, uri);
        this.requestBodyAdapter = requestBodyAdapter;
        this.requestBody = new RequestBody (this);
        headers = new HashMap<String, String> ();
        this.dataExtractor = dataExtractor;
        this.mappingResponseExtractor = mappingResponseExtractor;
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        String uri,
        InReturnAdapterSpecification<RequestBody, String> requestBodyAdapter,
        RequestBody requestBody,
        ResponseExtractor<Data> dataExtractor,
        ResponseExtractor<MappingResponse> mappingResponseExtractor
    ) {
        super(restTemplate, uri);
        this.requestBodyAdapter = requestBodyAdapter;
        this.requestBody = requestBody;
        headers = new HashMap<String, String> ();
        this.dataExtractor = dataExtractor;
        this.mappingResponseExtractor = mappingResponseExtractor;
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        InReturnAdapterSpecification<RequestBody, String> requestBodyAdapter,
        ResponseExtractor<Data> dataExtractor,
        ResponseExtractor<MappingResponse> mappingResponseExtractor
    ) {
        super(restTemplate, uriBuilder);
        this.requestBodyAdapter = requestBodyAdapter;
        this.requestBody = new RequestBody (this);
        headers = new HashMap<String, String> ();
        this.dataExtractor = dataExtractor;
        this.mappingResponseExtractor = mappingResponseExtractor;
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        InReturnAdapterSpecification<RequestBody, String> requestBodyAdapter,
        RequestBody requestBody,
        ResponseExtractor<Data> dataExtractor,
        ResponseExtractor<MappingResponse> mappingResponseExtractor
    ) {
        super(restTemplate, uriBuilder);
        this.requestBodyAdapter = requestBodyAdapter;
        this.requestBody = requestBody;
        headers = new HashMap<String, String> ();
        this.dataExtractor = dataExtractor;
        this.mappingResponseExtractor = mappingResponseExtractor;
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<RequestKey> cache,
        InReturnAdapterSpecification<RequestBody, String> requestBodyAdapter,
        ResponseExtractor<Data> dataExtractor,
        ResponseExtractor<MappingResponse> mappingResponseExtractor
    ) {
        super(restTemplate, uri, cache);
        this.requestBodyAdapter = requestBodyAdapter;
        this.requestBody = new RequestBody (this);
        headers = new HashMap<String, String> ();
        this.dataExtractor = dataExtractor;
        this.mappingResponseExtractor = mappingResponseExtractor;
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<RequestKey> cache,
        InReturnAdapterSpecification<RequestBody, String> requestBodyAdapter,
        RequestBody requestBody,
        ResponseExtractor<Data> dataExtractor,
        ResponseExtractor<MappingResponse> mappingResponseExtractor
    ) {
        super(restTemplate, uri, cache);
        this.requestBodyAdapter = requestBodyAdapter;
        this.requestBody = requestBody;
        headers = new HashMap<String, String> ();
        this.dataExtractor = dataExtractor;
        this.mappingResponseExtractor = mappingResponseExtractor;
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<RequestKey> cache,
        InReturnAdapterSpecification<RequestBody, String> requestBodyAdapter,
        ResponseExtractor<Data> dataExtractor,
        ResponseExtractor<MappingResponse> mappingResponseExtractor
    ) {
        super(restTemplate, uriBuilder, cache);
        this.requestBodyAdapter = requestBodyAdapter;
        this.requestBody = new RequestBody (this);
        headers = new HashMap<String, String> ();
        this.dataExtractor = dataExtractor;
        this.mappingResponseExtractor = mappingResponseExtractor;
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<RequestKey> cache,
        InReturnAdapterSpecification<RequestBody, String> requestBodyAdapter,
        RequestBody requestBody,
        ResponseExtractor<Data> dataExtractor,
        ResponseExtractor<MappingResponse> mappingResponseExtractor
    ) {
        super(restTemplate, uriBuilder, cache);
        this.requestBodyAdapter = requestBodyAdapter;
        this.requestBody = requestBody;
        headers = new HashMap<String, String> ();
        this.dataExtractor = dataExtractor;
        this.mappingResponseExtractor = mappingResponseExtractor;
    }

    public RequestBody getRequestBody() {
        return requestBody;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    @Override
    public QueryBuilder withApiKey (String apiKey) {

        headers.put(X_OPENFIGI_APIKEY, apiKey);

        return this;
    }

    public static final String OPENFIGI_APIKEY = "OPENFIGI_APIKEY";

    @Override
    public QueryBuilder withExternalApiKey() {
        return withApiKey (getProperty (OPENFIGI_APIKEY));
    }

    @Override
    public QueryBuilder withExternalApiKey(String key) {
        return withApiKey (getProperty (key));
    }

//    @Override
    protected RequestKey getKey() {
        return new RequestKey (headers, requestBody);
    }

    @Override
    public RequestKey getCacheKey () {
    	return getKey ();
    }

    public static final String MAPPING = "mapping";

    /**
     * Adds /mapping/ to the existing path. Note that the current default URL includes mapping already so
     * this method should only be necessary if the default url is shortened.
     */
    public QueryBuilder withMapping () {
        return (QueryBuilder) extendPathWith(MAPPING);
    }

    public static final String VALUES = "values";

    /**
     * @param key One of idType, exchCode, micCode, currency, marketSecDes, securityType, and securityType2.
     */
    public QueryBuilder withValues (String key) {

        extendPathWith(VALUES);
        extendPathWith(key);

        return this;
    }

    /**
     * @param key One of idType, exchCode, micCode, currency, marketSecDes, securityType, and securityType2.
     */
    public QueryBuilder withValues (Key key) {

        extendPathWith(VALUES);
        extendPathWith(key.toString());

        return this;
    }

    /**
     * Convenience method which returns the equivalent of:
     *
     * withValues(Key.idType);
     *
     * @param key The key being passed.
     */
    public QueryBuilder withValuesAsIdType () {
        return withValues (Key.idType);
    }

    /**
     * Convenience method which returns the equivalent of:
     *
     * withValues(Key.currency);
     *
     * @param key The key being passed.
     */
    public QueryBuilder withValuesAsCurrency () {
        return withValues (Key.currency);
    }

    /**
     * Convenience method which returns the equivalent of:
     *
     * withValues(Key.currency);
     *
     * @param key The key being passed.
     */
    public QueryBuilder withValuesAsExchCode () {
        return withValues (Key.exchCode);
    }

    /**
     * Convenience method which returns the equivalent of:
     *
     * withValues(Key.micCode);
     *
     * @param key The key being passed.
     */
    public QueryBuilder withValuesAsMicCode () {
        return withValues (Key.micCode);
    }

    /**
     * Convenience method which returns the equivalent of:
     *
     * withValues(Key.marketSecDes);
     *
     * @param key The key being passed.
     */
    public QueryBuilder withValuesAsMarketSecDes () {
        return withValues (Key.marketSecDes);
    }

    /**
     * Convenience method which returns the equivalent of:
     *
     * withValues(Key.securityType);
     *
     * @param key The key being passed.
     */
    public QueryBuilder withValuesAsSecurityType () {
        return withValues (Key.securityType);
    }

    /**
     * Convenience method which returns the equivalent of:
     *
     * withValues(Key.securityType2);
     *
     * @param key The key being passed.
     */
    public QueryBuilder withValuesAsSecurityType2 () {
        return withValues (Key.securityType2);
    }

    /**
     * Convenience method which returns the equivalent of:
     *
     * withMapping ().withValues(key);
     *
     * @param values The values being passed.
     */
    public QueryBuilder withMappingValues (String key) {
        return withMapping ().withValues(key);
    }

    /**
     * Convenience method which returns the equivalent of:
     *
     * withMapping ().withValues(key);
     *
     * @param key The key being passed.
     */
    public QueryBuilder withMappingValues (Key key) {
        return withMapping ().withValues(key);
    }

    /**
     * Convenience method which returns the equivalent of:
     *
     * withMapping ().withValues(Key.idType);
     *
     * @param key The key being passed.
     */
    public QueryBuilder withMappingValuesAsIdType () {
        return withMappingValues (Key.idType);
    }

    /**
     * Convenience method which returns the equivalent of:
     *
     * withMapping ().withValues(Key.currency);
     *
     * @param key The key being passed.
     */
    public QueryBuilder withMappingValuesAsCurrency () {
        return withMappingValues (Key.currency);
    }

    /**
     * Convenience method which returns the equivalent of:
     *
     * withMapping ().withValues(Key.currency);
     *
     * @param key The key being passed.
     */
    public QueryBuilder withMappingValuesAsExchCode () {
        return withMappingValues (Key.exchCode);
    }

    /**
     * Convenience method which returns the equivalent of:
     *
     * withMapping ().withValues(Key.micCode);
     *
     * @param key The key being passed.
     */
    public QueryBuilder withMappingValuesAsMicCode () {
        return withMappingValues (Key.micCode);
    }

    /**
     * Convenience method which returns the equivalent of:
     *
     * withMapping ().withValues(Key.marketSecDes);
     *
     * @param key The key being passed.
     */
    public QueryBuilder withMappingValuesAsMarketSecDes () {
        return withMappingValues (Key.marketSecDes);
    }

    /**
     * Convenience method which returns the equivalent of:
     *
     * withMapping ().withValues(Key.securityType);
     *
     * @param key The key being passed.
     */
    public QueryBuilder withMappingValuesAsSecurityType () {
        return withMappingValues (Key.securityType);
    }

    /**
     * Convenience method which returns the equivalent of:
     *
     * withMapping ().withValues(Key.securityType2);
     *
     * @param key The key being passed.
     */
    public QueryBuilder withMappingValuesAsSecurityType2 () {
        return withMappingValues (Key.securityType2);
    }

    @Override
    protected <T> T doExecute(Class<T> type) {

        T result = (T) getCache().get(getKey ());

        if (result == null) {

            if (MappingResponse.class.equals(type)) {
                result = (T) getRestTemplate().execute(
                    getEscapedURI(),
                    HttpMethod.GET,
                    new RequestCallback() {
                        @Override
                        public void doWithRequest(ClientHttpRequest request) throws IOException {
                        }
                    },
                    mappingResponseExtractor
                );
            } else {

                result = (T) getRestTemplate().execute(
                    getEscapedURI(),
                    HttpMethod.POST,
                    new RequestCallback() {

                        @Override
                        public void doWithRequest(ClientHttpRequest request) throws IOException {

                            HttpHeaders headers = request.getHeaders();

                            headers.setContentType(MediaType.APPLICATION_JSON);

                            for (Entry<String, String> nextEntry : getHeaders ().entrySet()) {

                                String key = nextEntry.getKey();
                                String value = nextEntry.getValue();

                                headers.add(key, value);
                            }

                            String requestBodyJson = requestBodyAdapter.adapt(getRequestBody());

                            log.debug("headers: " + headers + ", requestBodyJson: " + requestBodyJson);

                            request.getBody().write(requestBodyJson.getBytes());
                        }
                    },
                    dataExtractor
                );
            }
        }

        return result;
    }

    /**
     * Do get as {@link Data} and then return that result.
     */
    public Data doGetAsData () {
        return doGetAsData(data -> { return data; });
    }

    /**
     * Do get as {@link Data}, execute the given function, and then return an instance of type
     * {@link GenericResponse}.
     */
    public Data doGetAsData (Function<Data, Data> function) {
        return doGetAsData(Data.class, function);
    }

    /**
     * Do get as {@link Data}, execute the given function, and then return an instance of type resultType.
     */
    public <R> R doGetAsData (Class<R> resultType, Function<Data, R> function) {

        Data identify = doGet(Data.class);

        R result = function.apply(identify);

        return result;
    }

    /**
     * Do get as {@link MappingResponse} and then return that result.
     */
    public MappingResponse doGetAsMappingResponse () {
        return doGetAsMappingResponse(MappingResponse -> { return MappingResponse; });
    }

    /**
     * Do get as {@link MappingResponse}, execute the given function, and then return an instance of type
     * {@link GenericResponse}.
     */
    public MappingResponse doGetAsMappingResponse (Function<MappingResponse, MappingResponse> function) {
        return doGetAsMappingResponse(MappingResponse.class, function);
    }

    /**
     * Do get as {@link MappingResponse}, execute the given function, and then return an instance of type
     * resultType.
     */
    public <R> R doGetAsMappingResponse (Class<R> resultType, Function<MappingResponse, R> function) {

        MappingResponse mappingResponse = doGet(MappingResponse.class);

        R result = function.apply(mappingResponse);

        return result;
    }

    @Override
    public String toString() {
        return "QueryBuilder [requestBody=" + requestBody + ", headers=" + headers + ", requestBodyAdapter="
            + requestBodyAdapter + ", dataExtractor=" + dataExtractor + ", toString()=" + super.toString()
            + "]";
    }
}

class RequestKey implements Serializable {

    private static final long serialVersionUID = 3061141466398240807L;

    private final Map<String, String> headers;

    private final RequestBody requestBody;

    public RequestKey(Map<String, String> headers, RequestBody requestBody) {
        this.headers = headers;
        this.requestBody = requestBody;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public RequestBody getRequestBody() {
        return requestBody;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((headers == null) ? 0 : headers.hashCode());
        result = prime * result + ((requestBody == null) ? 0 : requestBody.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RequestKey other = (RequestKey) obj;
        if (headers == null) {
            if (other.headers != null)
                return false;
        } else if (!headers.equals(other.headers))
            return false;
        if (requestBody == null) {
            if (other.requestBody != null)
                return false;
        } else if (!requestBody.equals(other.requestBody))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Request [headers=" + headers + ", requestBody=" + requestBody + "]";
    }
}