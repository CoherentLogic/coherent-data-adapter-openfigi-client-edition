package com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain;

/**
 * Market sector description of the desired instrument(s).
 * <a href="https://openfigi.com/api/enumValues/v3/marketSecDes">Click here</a> for the current list of marketSecDes
 * values.
 *
 * For the following:
 *
 * 6 use _6
 * Multi-Asset use Multi_Asset2
 * Multi-asset use Multi_asset2
 *
 * @see <a href="https://openfigi.com/api/enumValues/v3/marketSecDes">OpenFIGI Market sector descriptions</a>
 * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public enum MarketSecDes {

    _6 ("6"),
    Alternatives ("Alternatives"),
    CAD_TBills ("CAD TBills"),
    Comdty ("Comdty"),
    Commodities ("Commodities"),
    Corp ("Corp"),
    Credit ("Credit"),
    CROSS_ASSET ("CROSS ASSET"),
    Cross_Asset ("Cross Asset"),
    Curncy ("Curncy"),
    Equity ("Equity"),
    Fixed_Income ("Fixed Income"),
    Foreign_Exchange ("Foreign Exchange"),
    Govt ("Govt"),
    Index ("Index"),
    Interest_Rate ("Interest Rate"),
    M_Mkt ("M-Mkt"), //
    Macroeconomic ("Macroeconomic"),
    Money_Market ("Money Market"),
    Mtge ("Mtge"),
    Multi_Asset ("Multi Asset"),
    Multi_asset ("Multi asset"),
    Multi_Assets ("Multi Assets"),
    Multi_Asset2 ("Multi-Asset"),//
    Multi_asset2 ("Multi-asset"),//
    Muni ("Muni"),
    Mutual_Funds ("Mutual Funds"),
    NA ("N/A"), //
    Pfd ("Pfd"),
    Trd_Strat ("Trd Strat");

    private final String marketSecDes;

    MarketSecDes(final String marketSecDes) {
        this.marketSecDes = marketSecDes;
    }

    @Override
    public String toString() {
        return marketSecDes;
    }
}
