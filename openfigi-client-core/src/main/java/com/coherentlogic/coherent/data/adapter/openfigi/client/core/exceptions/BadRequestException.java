package com.coherentlogic.coherent.data.adapter.openfigi.client.core.exceptions;

import org.springframework.core.NestedRuntimeException;

public class BadRequestException extends NestedRuntimeException {

    private static final long serialVersionUID = 2509726928656483246L;

    public BadRequestException(String msg) {
        super(msg);
    }

    public BadRequestException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
