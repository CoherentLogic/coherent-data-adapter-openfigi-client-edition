package com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain;

/**
 * Specification for intervals.
 *
 * @see {@link NumericInterval}
 * @see {@link DateInterval}
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public interface IntervalSpecification<T> {

    /**
     * Method validates that the given interval is valid.
     * 
     * @throws If the validation fails 
     * @return
     */
    <X extends AbstractInterval<T>> X validate();
}
