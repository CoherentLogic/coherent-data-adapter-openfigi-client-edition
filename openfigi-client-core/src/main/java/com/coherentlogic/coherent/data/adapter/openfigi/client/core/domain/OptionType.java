package com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain;

/**
 * Will filter instruments based on option type. Valid values are Call and Put.
 *
 * @since 2.0.3.1-RELEASE
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public enum OptionType {
    Call, Put;
}
