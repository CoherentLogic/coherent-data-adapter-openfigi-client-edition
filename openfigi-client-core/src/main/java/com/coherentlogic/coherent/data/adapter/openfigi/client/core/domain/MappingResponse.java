package com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * Holds the successful result of a call to the
 * <a href="https://openfigi.com/api#get-v3-mapping-values">OpenFIGI V3 mapping value web service endpoint</a>.
 *
 * {
 *     "values":
 *         [
 *             "6",
 *             "Alternatives",
 *             "CAD TBills",
 *             "Comdty",
 *             "Commodities",
 *             "Corp",
 *             "Credit",
 *             "CROSS ASSET",
 *             "Cross Asset",
 *             "Curncy",
 *             "Equity",
 *             "Fixed Income",
 *             "Foreign Exchange",
 *             "Govt",
 *             "Index",
 *             "Interest Rate",
 *             "M-Mkt",
 *             "Macroeconomic",
 *             "Money Market",
 *             "Mtge",
 *             "Multi Asset",
 *             "Multi asset",
 *             "Multi Assets",
 *             "Multi-Asset",
 *             "Multi-asset",
 *             "Muni",
 *             "Mutual Funds",
 *             "N/A",
 *             "Pfd",
 *             "Trd Strat"
 *         ]
 * }
 *
 * @see <a href="https://api.openfigi.com/v3/mapping/values/marketSecDes">enumValues/v3/marketSecDes</a>
 * @see <a href="https://www.openfigi.com/api">The OpenFIGI API</a>
 * @see <a href="https://openfigi.com/assets/local/figi-allocation-rules.pdf">FIGI Allocation Rules</a>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=MappingResponse.MAPPING_RESPONSE)
@Visitable
public class MappingResponse extends SerializableBean {

    private static final long serialVersionUID = 3953287202001264212L;

    public static final String MAPPING_RESPONSE = "mappingResponse", VALUES = "values";

    public static final String MAPPING_VALUES_LIST_PROPERTY = "mappingValuesList";

    @Visitable
    private List<MappingValue> mappingValueList;

    @OneToMany(cascade=CascadeType.ALL)
    public List<MappingValue> getMappingValueList () {
        return mappingValueList;
    }

    public MappingValue addMappingValue (MappingValue mappingValue) {

        if (mappingValueList == null)
            mappingValueList = new ArrayList<MappingValue> ();

        mappingValueList.add(mappingValue);

        return mappingValue;
    }

    public void setMappingValueList(List<MappingValue> mappingValueList) {

        List<MappingValue> oldValue = this.mappingValueList;

        this.mappingValueList = mappingValueList;

        firePropertyChange(MAPPING_VALUES_LIST_PROPERTY, oldValue, mappingValueList);
    }

    public Iterator<MappingValue> valuesIterator () {
        return mappingValueList.iterator();
    }

    public void forEachMappingValue (Consumer<MappingValue> consumer) {
        mappingValueList.forEach(consumer);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((mappingValueList == null) ? 0 : mappingValueList.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        MappingResponse other = (MappingResponse) obj;
        if (mappingValueList == null) {
            if (other.mappingValueList != null)
                return false;
        } else if (!mappingValueList.equals(other.mappingValueList))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "MappingResponse [values=" + mappingValueList + "]";
    }
}
