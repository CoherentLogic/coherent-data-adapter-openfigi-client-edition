package com.coherentlogic.coherent.data.adapter.openfigi.client.core.factories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.coherentlogic.coherent.data.adapter.core.factories.TypedFactory;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.DataEntry;

/**
 * @deprecated No longer used as the bean is defined in the Global configuration and this then becomes
 *  unnecessary.
 */
public class DataEntryFactory implements TypedFactory<DataEntry> {

    public static final String BEAN_NAME = "dataEntryFactory";

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public DataEntry getInstance() {
        return applicationContext.getBean(DataEntry.class);
    }
}
