package com.coherentlogic.coherent.data.adapter.openfigi.client.core.extractors;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseExtractor;

import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.MappingResponse;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;

public class MappingResponseExtractor extends AbstractResponseExtractor
    implements ResponseExtractor<MappingResponse> {

    private static final Logger log = LoggerFactory.getLogger(MappingResponseExtractor.class);

    public static final String BEAN_NAME = "mappingResponseExtractor";

    private final GsonBuilder gsonBuilder;

    public MappingResponseExtractor (
        GsonBuilder gsonBuilder,
        TypeAdapter<MappingResponse> mappingResponseTypeAdapter
    ) {
        this.gsonBuilder = gsonBuilder;
        gsonBuilder.registerTypeAdapter(MappingResponse.class, mappingResponseTypeAdapter);
    }

    @Override
    public MappingResponse extractData(ClientHttpResponse response) throws IOException {

//        if (HttpStatus.BAD_REQUEST.equals(response.getStatusCode()))
//            throw new BadRequestException ("Details include: rawStatusCode: "
//                + response.getStatusCode().toString() + ", statusText: " + response.getStatusText());
//
//        InputStream in = response.getBody();
//
//        String json = IOUtils.toString(in, StandardCharsets.UTF_8);
//
//        log.debug("json: " + json);

        String json = extractBody(response);

        MappingResponse result = gsonBuilder.create().fromJson(json, MappingResponse.class);

        return result;
    }
}
