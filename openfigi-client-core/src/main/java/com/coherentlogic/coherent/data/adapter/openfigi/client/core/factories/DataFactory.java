package com.coherentlogic.coherent.data.adapter.openfigi.client.core.factories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.coherentlogic.coherent.data.adapter.core.factories.TypedFactory;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.Data;

/**
 * @deprecated No longer used as the bean is defined in the Global configuration and this then becomes
 *  unnecessary.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class DataFactory implements TypedFactory<Data> {

    public static final String BEAN_NAME = "dataFactory";

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public Data getInstance() {
        return applicationContext.getBean(Data.class);
    }
}
