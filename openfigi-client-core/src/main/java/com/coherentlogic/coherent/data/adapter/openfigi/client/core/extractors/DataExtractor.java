package com.coherentlogic.coherent.data.adapter.openfigi.client.core.extractors;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseExtractor;

import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.Data;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class DataExtractor extends AbstractResponseExtractor implements ResponseExtractor<Data> {

    private static final Logger log = LoggerFactory.getLogger(DataExtractor.class);

    public static final String BEAN_NAME = "dataExtractor";

    private final GsonBuilder gsonBuilder;

    public DataExtractor (GsonBuilder gsonBuilder, TypeAdapter<Data> dataTypeAdapter) {
        this.gsonBuilder = gsonBuilder;
        gsonBuilder.registerTypeAdapter(Data.class, dataTypeAdapter);
    }

    @Override
    public Data extractData(ClientHttpResponse response) throws IOException {

//        InputStream in = response.getBody();
//
//        String json = IOUtils.toString(in, StandardCharsets.UTF_8);
//
//        log.debug("json: " + json);

        String json = extractBody(response);

        Data result = gsonBuilder.create().fromJson(json, Data.class);

        return result;
    }
}
