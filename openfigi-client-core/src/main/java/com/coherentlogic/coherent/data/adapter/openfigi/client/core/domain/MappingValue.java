package com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * An entry in a {@link MappingResponse}.
 *
 * @see <a href="https://www.openfigi.com/api">The OpenFIGI API</a>
 * @see <a href="https://openfigi.com/assets/local/figi-allocation-rules.pdf">FIGI Allocation Rules</a>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=MappingValue.MAPPING_VALUE)
@Visitable
public class MappingValue extends SerializableBean {

    private static final long serialVersionUID = -154259107188957382L;

    public static final String MAPPING_VALUE = "mappingValue";

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        MappingValue other = (MappingValue) obj;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "MappingValue [value=" + value + "]";
    }
}
