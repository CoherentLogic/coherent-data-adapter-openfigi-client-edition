package com.coherentlogic.coherent.data.adapter.openfigi.client.core.exceptions;

import org.springframework.core.NestedRuntimeException;

import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.AbstractInterval;

/**
 * An exception that is thrown when an {@link AbstractInterval} is invalid -- for example when a start date comes after
 * an end date.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class InvalidIntervalRuntimeException extends NestedRuntimeException {

    private static final long serialVersionUID = 7758262896323978577L;

    public InvalidIntervalRuntimeException(Object target) {
        super("Entity name resolution failed for the object: " + target);
    }
}
