package com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain;

/**
 * A bounded numeric interval.
 *
 * <b>strike</b> : Array(2) / Will find instruments whose strike price falls in an interval. Value should be
 * an Array interval of the form [a, b] where a, b are Numbers or null. At least one entry must be a Number.
 * When both are Numbers, it is required that a <= b. Also, [a, null] is equivalent to the interval
 * [a, infinity) and [null, b] is equivalent to (-infinity, b].
 *
 * <b>contractSize</b> : Array(2) / Will find instruments whose contract size falls in an interval. Value
 * should be an Array interval of the form [a, b] where a, b are Numbers or null. At least one entry must be
 * a Number. When both are Numbers, it is required that a <= b. Also, [a, null] is equivalent to the interval
 * [a, infinity) and [null, b] is equivalent to (-infinity, b].
 * 
 * <b>coupon</b> : Array(2) / Will find instruments whose coupon falls in an interval. Value should be an
 * Array interval of the form [a, b] where a, b are Numbers or null. At least one entry must be a Number.
 * When both are Numbers, it is required that a <= b. Also, [a, null] is equivalent to the interval
 * [a, infinity) and [null, b] is equivalent to (-infinity, b].
 *
 * <b>expiration</b> : Array(2) / When securityType2 is Option or Warrant / Will find instruments whose
 * expiration date falls in an interval. Value should be an Array interval of the form [a, b] where a, b are
 * date Strings or null. Date strings must be of the form YYYY-MM-DD. At least one entry must be a date
 * String. When both are date Strings, it is required that a and b are no more than one year apart. Also,
 * [a, null] is equivalent to the interval [a, a + (1 year)] and [null, b] is equivalent to
 * [b - (1 year), b].
 *
 * <b>maturity</b> : Array(2) / When securityType2 is Pool / Will find instruments whose maturity date falls
 * in an interval. Value should be an Array interval of the form [a, b] where a, b are date Strings or null.
 * Date strings must be of the form YYYY-MM-DD. At least one entry must be a date String. When both are date
 * Strings, it is required that a and b are no more than one year apart. Also, [a, null] is equivalent to the
 * interval [a, a + (1 year)] and [null, b] is equivalent to [b - (1 year), b].
 *
 * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class NumericInterval extends AbstractInterval<Number> {

    public NumericInterval(Number aValue) {
        super(aValue);
    }

    public NumericInterval(Number aValue, Number bValue) {
        super(aValue, bValue);
    }

    @Override
    public NumericInterval validate() {
        // strike must be an array of length 2.
        return this;
    }
}
