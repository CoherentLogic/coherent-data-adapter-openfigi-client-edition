package com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain;

/**
 * The name of the Mapping Job property for which to list the possible values.
 *
 * @see <a href="https://openfigi.com/api#get-v3-mapping-values">GET /v3/mapping/values/:key</a>
 * @see <a href="https://www.openfigi.com/api">The OpenFIGI API</a>
 * @see <a href="https://openfigi.com/assets/local/figi-allocation-rules.pdf">FIGI Allocation Rules</a>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public enum Key {

    idType,
    exchCode,
    micCode,
    currency,
    marketSecDes,
    securityType,
    securityType2;
}
