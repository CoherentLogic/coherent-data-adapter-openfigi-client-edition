package com.coherentlogic.coherent.data.adapter.openfigi.client.embedded;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.Data;

//@SpringBootApplication(exclude= {org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration.class})
//@EnableAutoConfiguration
@ComponentScan(basePackages= {"com.coherentlogic.coherent.data.adapter.openfigi.client.core.configuration"})
//@ImportResource({
//    "classpath*:openfigi-client-configuration/api-key-beans.xml",
//    "classpath*:openfigi-client-configuration/application-context.xml"
//})
public class EmbeddedOpenFIGIClient {

    public static ConfigurableApplicationContext initialize () {

        SpringApplicationBuilder builder = new SpringApplicationBuilder(EmbeddedOpenFIGIClient.class);

        ConfigurableApplicationContext applicationContext =
            builder.web(false).headless(true).registerShutdownHook(true).run(new String[]{});

        return applicationContext;
    }

    public static final void main (String[] unused) {

        Data result = initialize ()
            .getBean(com.coherentlogic.coherent.data.adapter.openfigi.client.core.builders.QueryBuilder.class)
            .getRequestBody()
            .withFinancialInstrumentGlobalIdentifier("BBG00K62SK81")
            .done()
            .doGetAsData();

        System.out.println ("result: " + result);
    }
}
