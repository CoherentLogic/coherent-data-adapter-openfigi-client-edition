package com.coherentlogic.coherent.data.adapter.openfigi.client.core.builders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.coherentlogic.coherent.data.adapter.openfigi.client.core.configuration.DefaultIntegrationTestConfiguration;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.configuration.XMLConfiguration;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.Currency;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.Data;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.DataEntry;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.ErrorEntry;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.ExchCode;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.IdType;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.MappingResponse;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.MappingValue;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.MarketSecDes;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.MicCode;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.SecurityType2;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.exceptions.BadRequestException;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * @author <a href="support@coherentlogic.com">Support</a>
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes={DefaultIntegrationTestConfiguration.class, XMLConfiguration.class})
//@SpringBootApplication(scanBasePackages = {"com.coherentlogic.coherent.data.adapter.openfigi"})
public class QueryBuilderTest {

    public static final String FOO = "foo";

    @Autowired
    private QueryBuilder queryBuilder;

    @AfterEach
    public void tearDown () {
        this.queryBuilder = null;
    }

    @Test
    public void testGetWithValidRequest1 () {

        /* NOTE: If we don't use the API key we'll receive the following response:
         *
         *       429 Too Many Requests
         */
        Data data = queryBuilder
            .withExternalApiKey()
            .getRequestBody()
                .withWertpapier("851399")
            .done()
        .doGetAsData();

        assertNotNull (data);
        assertTrue(1 <= data.getEntries().size());

        List<? extends SerializableBean> dataEntries = data.getEntries().get(0);

        for (SerializableBean next : dataEntries) {
            assertTrue (next instanceof DataEntry);
        }
    }

    @Test
    public void testGetWithValidRequest2 () {

        Data data = queryBuilder
            .withExternalApiKey()
            .getRequestBody()
                .newMappingEntry()
                    .withIdType("ID_WERTPAPIER")
                    .withIdValue("851399")
                .done()
            .done()
        .doGetAsData();

        assertNotNull (data);
        assertTrue(1 <= data.getEntries().size());

        List<? extends SerializableBean> dataEntries = data.getEntries().get(0);

        for (SerializableBean next : dataEntries)
            assertTrue (next instanceof DataEntry);
    }

    @Test
    public void testGetWithLargeValidRequest () {

        Data data = queryBuilder
            .withExternalApiKey()
            .getRequestBody()
                .newMappingEntry()
                    .withIdType("ID_ISIN")
                    .withIdValue("US4592001014")
                .done()
                .newMappingEntry()
                    .withIdType("ID_WERTPAPIER")
                    .withIdValue("851399")
                .done()
            .done()
        .doGetAsData();

        assertNotNull (data);
        assertTrue(2 <= data.getEntries().size());
    }

    /**
     * 
     * "Rate-Limiting
     *
     * User is subject to rate-limiting while using the OpenFIGI API. Please note that the limits are approximations and
     * may be subject to change. There are two types of limitation currently enforced:
     *
     * Types of limitation                             Without API_KEY    With API_KEY
     * Number of requests you could make per minute    5                  250
     * Number of mapping jobs in a single request      5                  100
     * Total number of mapping jobs per minute         25                 25,000"
     * 
     * @see <a href="https://openfigi.com/api">OpenFIGI API</a>
     *
     * Post to:
     *   https://api.openfigi.com/v3/mapping
     *
     * With header:
     *   Content-Type: text/json
     *
     * With body:
     *   [{"idType":"ID_WERTPAPIER","idValue":"851399","exchCode":"US"}]
     */
//    @Test
//    public void testExample1 () {
//
//        Data data = queryBuilder
//            .withExternalApiKey()
//            .getRequestBody()
//                .newMappingEntry()
//                    .withIdType("ID_WERTPAPIER")
//                    .withIdValue("851399")
//                    .withExchangeCode("US")
//                .done()
//            .done()
//        .doGetAsData();
//
//        System.out.println("data.size: " + data.getEntries().size());
//
//        data.getEntries().forEach(
//            entry -> {
//                System.out.println("entry: " + entry);
//            }
//        );
//    }

    @Test
    public void testGetWithLargeValidRequestAndOneInvalidRequest () {

        Data data = queryBuilder
            .withExternalApiKey()
            .getRequestBody()
                .newMappingEntry()
                    .withIdType("ID_ISIN")
                    .withIdValue("US4592001014")
                .done()
                .newMappingEntry()
                    .withIdType("ID_WERTPAPIER")
                    .withIdValue("851399")
                .done()
                .newMappingEntry()
                    .withIdType("ID_WERTPAPIER")
                    .withIdValue("INVALID")
                .done()
            .done()
        .doGetAsData();

        assertNotNull (data);

        List<List<? extends SerializableBean>> entries = data.getEntries();

        assertTrue(3 <= entries.size());

        DataEntry dataEntry = (DataEntry) entries.get(0).get(0);

        List<ErrorEntry> errorEntries = (List<ErrorEntry>) entries.get(2);

        ErrorEntry errorEntry = errorEntries.get(0);

        assertEquals("Invalid idValue format", errorEntry.getError());

        checkDataEntryPCLEventGeneration (dataEntry);
        checkErrorEntryPCLEventGeneration (errorEntry);
    }

    static void checkDataEntryPCLEventGeneration (DataEntry dataEntry) {

        AtomicBoolean flag = new AtomicBoolean();

        dataEntry.addPropertyChangeListener(
            event -> {
                flag.set(true);
            }
        );

        dataEntry.setCompositeFIGI(FOO);

        assertTrue (flag.get());

        flag.set(false);

        dataEntry.setExchangeCode(FOO);

        assertTrue (flag.get());

        flag.set(false);

        dataEntry.setFigi(FOO);

        assertTrue (flag.get());

        flag.set(false);

        dataEntry.setMarketSector(FOO);

        assertTrue (flag.get());

        flag.set(false);

        dataEntry.setName(FOO);

        assertTrue (flag.get());

        flag.set(false);

        dataEntry.setSecurityType(FOO);

        assertTrue (flag.get());

        flag.set(false);

        dataEntry.setShareClassFIGI(FOO);

        assertTrue (flag.get());

        flag.set(false);

        dataEntry.setTicker(FOO);

        assertTrue (flag.get());

        flag.set(false);

        dataEntry.setUniqueID(FOO);

        assertTrue (flag.get());

        flag.set(false);

        dataEntry.setUniqueIDForFutureOption(FOO);

        assertTrue (flag.get());
    }

    static void checkErrorEntryPCLEventGeneration (ErrorEntry errorEntry) {

        AtomicBoolean flag = new AtomicBoolean();

        errorEntry.addPropertyChangeListener(
            event -> {
                flag.set(true);
            }
        );

        errorEntry.setError(FOO);

        assertTrue (flag.get());
    }

    @Test
    public void testGetWithInvalidIdValue () {

        Data data = queryBuilder
            .withExternalApiKey()
            .getRequestBody()
                .newMappingEntry()
                    .withIdType("ID_WERTPAPIER")
                    .withIdValue("851399XXX")
                .done()
            .done()
        .doGetAsData();

        List<ErrorEntry> errorEntries = (List<ErrorEntry>) data.getEntries().get(0);

        assertNotNull (data);
        assertTrue(1 <= data.getEntries().size());
//        assertEquals("No identifier found.", errorEntries.get(0).getError());
    }

    /**
     * curl 'https://api.openfigi.com/v3/mapping' --request POST --header 'Content-Type: application/json' --data '[{"idType":"BASE_TICKER","idValue":"TSLA 10 C100","securityType2":"Option","expiration":["2018-10-01","2018-12-01"]}]'
     *
     * { "idType":"BASE_TICKER", "idValue":"TSLA 10 C100", "securityType2":"Option", "expiration":["2018-10-01", "2018-12-01"]}
     */
    @Test
    public void testGetWithValidRequestWithSecurityType2AsOptionAndExpirationSet () {

        Data data = queryBuilder
            .withExternalApiKey()
            .getRequestBody()
                .newMappingEntry()
                    .withIdType("BASE_TICKER")
                    .withIdValue("TSLA 10 C100")
                    .withSecurityType2("Option")
                    .withExpiration("2018-10-01", "2018-12-01")
                .done()
            .done()
        .doGetAsData();

        assertNotNull (data);
        assertTrue(1 <= data.getEntries().size());

        List<? extends SerializableBean> dataEntries = data.getEntries().get(0);

        assertTrue (60 < dataEntries.size());

        for (SerializableBean next : dataEntries)
            assertTrue (next instanceof DataEntry);
    }

    /**
     * curl 'https://api.openfigi.com/v3/mapping' --request POST --header 'Content-Type: application/json' --data '[{"idType":"BASE_TICKER","idValue":"FG","marketSecDes":"Mtge","securityType2":"Pool","maturity":["2019-09-01","2020-06-01"]}]'
     *
     * [{"idType":"BASE_TICKER","idValue":"FG","marketSecDes":"Mtge","securityType2":"Pool","maturity":["2019-09-01","2020-06-01"]]
     */
    @Test
    public void testGetWithValidRequestIncludingMaturity () {

        Data data = queryBuilder
            .withExternalApiKey()
            .getRequestBody()
                .newMappingEntry()
                    .withIdType("BASE_TICKER")
                    .withIdValue("FG")
                    .withMarketSectorDescription("Mtge")
                    .withSecurityType2("Pool")
                    .withMaturity("2019-09-01", "2020-06-01")
                .done()
            .done()
        .doGetAsData();

        assertNotNull (data);
        assertTrue(1 <= data.getEntries().size());

        List<? extends SerializableBean> dataEntries = data.getEntries().get(0);

        assertTrue (4000 < dataEntries.size());

        for (SerializableBean next : dataEntries)
            assertTrue (next instanceof DataEntry);
    }

    /**
     * curl 'https://api.openfigi.com/v3/mapping' --request POST --header 'Content-Type: application/json' --data '[{ "idType":"BASE_TICKER", "idValue":"NFLX 9 P330", "marketSecDes":"Equity", "securityType2":"Option", "strike":[330,null], "expiration":["2018-07-01",null]}]'
     * 
     * { "idType":"BASE_TICKER", "idValue":"NFLX 9 P330", "marketSecDes":"Equity", "securityType2":"Option", "strike":[330,null], "expiration":["2018-07-01",null]},
     */
    @Test
    public void testGetWithValidRequestIncludingStrike () {

        Data data = queryBuilder
            .withExternalApiKey()
            .getRequestBody()
                .newMappingEntry()
                    .withIdType("BASE_TICKER")
                    .withIdValue("NFLX 9 P330")
                    .withMarketSectorDescription("Equity")
                    .withSecurityType2("Option")
                    .withStrike(330, null)
                    .withExpiration("2018-07-01")
                .done()
            .done()
        .doGetAsData();

        assertNotNull (data);
        assertTrue(1 <= data.getEntries().size());

        List<? extends SerializableBean> dataEntries = data.getEntries().get(0);

        assertTrue (60 < dataEntries.size());

        for (SerializableBean next : dataEntries)
            assertTrue (next instanceof DataEntry);
    }

    /**
     * NOT TESTED: curl 'https://api.openfigi.com/v3/mapping' --request POST --header 'Content-Type: application/json' --data '[{ "idType":"BASE_TICKER", "idValue":"IBM", "marketSecDes":"Corp", "securityType2":"Corp", "maturity":["2026-11-01",null]}]'
     * 
     * { "idType":"BASE_TICKER", "idValue":"IBM", "marketSecDes":"Corp", "securityType2":"Corp", "maturity":["2026-11-01",null]},
     */
    @Test
    public void testGetWithValidRequestForIBM () {

        Data data = queryBuilder
            .withExternalApiKey()
            .getRequestBody()
                .newMappingEntry()
                    .withIdType("BASE_TICKER")
                    .withIdValue("IBM")
                    .withMarketSectorDescription("Corp")
                    .withSecurityType2("Corp")
                    .withMaturity("2026-11-01")
                .done()
            .done()
        .doGetAsData();

        assertNotNull (data);
        assertTrue(1 <= data.getEntries().size());

        List<? extends SerializableBean> dataEntries = data.getEntries().get(0);

        assertTrue (2 < dataEntries.size());

        for (SerializableBean next : dataEntries)
            assertTrue (next instanceof DataEntry);
    }

    /**
     * DO NOT USE THE V1 MAPPING FOR THIS! (only errors will be returned)
     *
     * curl 'https://api.openfigi.com/v3/mapping' --request POST --header 'Content-Type: application/json' --data '[{ "idType": "ID_ISIN", "idValue": "US4592001014" }]'
     *
     * { "idType": "ID_ISIN", "idValue": "US4592001014" }
     */
    @Test
    public void testGetWithValidRequestForUS4592001014 () {

        Data data = queryBuilder
            .withExternalApiKey()
            .getRequestBody()
                .newMappingEntry()
                    .withIdType("ID_ISIN")
                    .withIdValue("US4592001014")
                .done()
            .done()
        .doGetAsData();

        assertNotNull (data);
        assertTrue(1 <= data.getEntries().size());

        List<? extends SerializableBean> dataEntries = data.getEntries().get(0);

        assertTrue (165 < dataEntries.size());

        for (SerializableBean next : dataEntries)
            assertTrue (next instanceof DataEntry);
    }

    /**
     * DO NOT USE THE V1 MAPPING FOR THIS! (only errors will be returned)
     *
     * curl 'https://api.openfigi.com/v3/mapping' --request POST --header 'Content-Type: application/json' --data '[{ "idType": "ID_WERTPAPIER", "idValue": "851399", "exchCode": "US" }]'
     *
     * { "idType": "ID_WERTPAPIER", "idValue": "851399", "exchCode": "US" }
     */
    @Test
    public void testGetWithValidRequestFor851399 () {

        Data data = queryBuilder
            .withExternalApiKey()
            .getRequestBody()
                .newMappingEntry()
                    .withIdType("ID_WERTPAPIER")
                    .withIdValue("851399")
                    .withExchangeCode("US")
                .done()
            .done()
        .doGetAsData();

        assertNotNull (data);
        assertTrue(1 <= data.getEntries().size());

        List<? extends SerializableBean> dataEntries = data.getEntries().get(0);

        assertTrue (dataEntries.get(0) instanceof DataEntry);
    }

    /**
     * DO NOT USE THE V1 MAPPING FOR THIS! (only errors will be returned)
     *
     * curl 'https://api.openfigi.com/v3/mapping' --request POST --header 'Content-Type: application/json' --data '[{ "idType": "ID_BB_UNIQUE", "idValue": "EQ0010080100001000", "currency": "USD" }]'
     *
     * { "idType": "ID_BB_UNIQUE", "idValue": "EQ0010080100001000", "currency": "USD" }
     */
    @Test
    public void testGetWithValidRequestForEQ0010080100001000 () {

        Data data = queryBuilder
            .withExternalApiKey()
            .getRequestBody()
                .newMappingEntry()
                    .withIdType("ID_BB_UNIQUE")
                    .withIdValue("EQ0010080100001000")
                    .withCurrency("USD")
                .done()
            .done()
        .doGetAsData();

        assertNotNull (data);
        assertTrue(1 <= data.getEntries().size());

        List<? extends SerializableBean> dataEntries = data.getEntries().get(0);

        assertTrue (10 < dataEntries.size());

        for (SerializableBean next : dataEntries)
            assertTrue (next instanceof DataEntry);
    }

    /**
     * DO NOT USE THE V1 MAPPING FOR THIS! (only errors will be returned)
     *
     * curl 'https://api.openfigi.com/v3/mapping' --request POST --header 'Content-Type: application/json' --data '[{ "idType": "ID_SEDOL", "idValue": "2005973", "micCode": "EDGX", "currency": "USD" }]'
     *
     * { "idType": "ID_SEDOL", "idValue": "2005973", "micCode": "EDGX", "currency": "USD" }
     */
    @Test
    public void testGetWithValidRequestFor2005973 () {

        Data data = queryBuilder
            .withExternalApiKey()
            .getRequestBody()
                .newMappingEntry()
                    .withIdType("ID_SEDOL")
                    .withIdValue("2005973")
                    .withMarketIdentificationCode("EDGX")
                    .withCurrency("USD")
                .done()
            .done()
        .doGetAsData();

        assertNotNull (data);
        assertTrue(1 <= data.getEntries().size());

        List<? extends SerializableBean> dataEntries = data.getEntries().get(0);

        assertTrue (dataEntries.get(0) instanceof DataEntry);
    }

    /**
     * DO NOT USE THE V1 MAPPING FOR THIS! (only errors will be returned)
     *
     * curl 'https://api.openfigi.com/v3/mapping' --request POST --header 'Content-Type: application/json' --data '[{"idType":"BASE_TICKER", "idValue":"2251Q", "securityType2":"Common Stock", "includeUnlistedEquities": true}]'
     *
     * curl 'https://api.openfigi.com/v3/mapping' --request POST --header 'Content-Type: application/json' --header 'X-OPENFIGI-APIKEY: [Add your key here]' --data '[{"idType":"BASE_TICKER", "idValue":"2251Q", "securityType2":"Common Stock", "includeUnlistedEquities": true}]'
     *
     * { "idType":"BASE_TICKER", "idValue":"2251Q", "securityType2":"Common Stock", "includeUnlistedEquities": true}
     */
    @Test
    public void testGetWithValidRequestFor2251Q () {

        Data data = queryBuilder
            .withExternalApiKey()
            .getRequestBody()
                .newMappingEntry()
                    .withIdType("BASE_TICKER")
                    .withIdValue("2251Q")
                    .withSecurityType2("Common Stock")
                    .withIncludeUnlistedEquities(Boolean.TRUE)
                .done()
            .done()
        .doGetAsData();

        assertNotNull (data);
        assertTrue(1 <= data.getEntries().size());

        List<? extends SerializableBean> dataEntries = data.getEntries().get(0);

        assertTrue (20 < dataEntries.size());

        for (SerializableBean next : dataEntries)
            assertTrue (next instanceof DataEntry);
    }

    @Test
    public void testGetWithValidRequestForAllV3Examples () {

        Data data = queryBuilder
            .withExternalApiKey()
            .getRequestBody()
                .newMappingEntry()
                    .withIdTypeAndValue(IdType.ID_ISIN, "US4592001014")
                .done()
                .newMappingEntry()
                    .withIdTypeAndValue(IdType.ID_WERTPAPIER, "851399")
                    .withExchangeCode(ExchCode.US)
                .done()
                .newMappingEntry()
                    .withIdTypeAndValue(IdType.ID_BB_UNIQUE, "EQ0010080100001000")
                    .withCurrency(Currency.USD)
                .done()
                .newMappingEntry()
                    .withIdTypeAndValue(IdType.ID_SEDOL, "2005973")
                    .withMarketIdentificationCode(MicCode.EDGX)
                    .withCurrency(Currency.USD)
                .done()
                .newMappingEntry()
                    .withIdTypeAndValue(IdType.BASE_TICKER, "TSLA 10 C100")
                    .withSecurityType2(SecurityType2.Option)
                    .withExpiration("2018-10-01", "2018-12-01")
                .done()
                .newMappingEntry()
                    .withIdTypeAndValue(IdType.BASE_TICKER, "NFLX 9 P330")
                    .withMarketSectorDescription(MarketSecDes.Equity)
                    .withSecurityType2(SecurityType2.Option)
                    .withStrike(330)
                    .withExpiration("2018-07-01")
                .done()
                .newMappingEntry()
                    .withIdTypeAndValue(IdType.BASE_TICKER, "FG")
                    .withMarketSectorDescription(MarketSecDes.Mtge)
                    .withSecurityType2(SecurityType2.Pool)
                    .withMaturity("2019-09-01", "2020-06-01")
                .done()
                .newMappingEntry()
                    .withIdTypeAndValue(IdType.BASE_TICKER, "IBM")
                    .withMarketSectorDescription(MarketSecDes.Corp)
                    .withSecurityType2(SecurityType2.Corp)
                    .withMaturity("2026-11-01")
                .done()
                .newMappingEntry()
                    .withIdTypeAndValue(IdType.BASE_TICKER, "2251Q")
                    .withSecurityType2(SecurityType2.Common_Stock)
                    .withIncludeUnlistedEquities(Boolean.TRUE)
                .done()
            .done()
        .doGetAsData();

        assertNotNull (data);
        assertTrue(1 <= data.getEntries().size());

        List<? extends SerializableBean> dataEntries = data.getEntries().get(0);

        // This should return exactly 168 entries even though some of the examples above return more than 168 (4000+ in
        // one case).
        assertTrue (165 < dataEntries.size());

        for (SerializableBean next : dataEntries)
            assertTrue (next instanceof DataEntry);
    }

    /**
     * 
     */
    @Disabled("This test can cause a '429 Too Many Requests' return code so it's being disabled and should be run manually.")
    @Test
    public void testGetMappingResponseForIdTypeMappingValues () {

        MappingResponse response = queryBuilder
            .withExternalApiKey()
            .withValuesAsIdType()
            .doGetAsMappingResponse ();

        assertNotNull (response);
        assertNotNull (response.getMappingValueList());

        /* BASE_TICKER,
         * COMPOSITE_ID_BB_GLOBAL,
         * ID_BB,
         * ID_BB_GLOBAL,
         * ID_BB_GLOBAL_SHARE_CLASS_LEVEL,
         * ID_BB_SEC_NUM_DES,
         * ID_BB_UNIQUE,
         * ID_CINS,
         * ID_COMMON,
         * ID_CUSIP,
         * ID_CUSIP_8_CHR,
         * ID_EXCH_SYMBOL,
         * ID_FULL_EXCHANGE_SYMBOL,
         * ID_ISIN,
         * ID_ITALY,
         * ID_SEDOL,
         * ID_SHORT_CODE,
         * ID_WERTPAPIER,
         * OCC_SYMBOL,
         * OPRA_SYMBOL,
         * TICKER,
         * TRADING_SYSTEM_IDENTIFIER,
         * UNIQUE_ID_FUT_OPT
         */
        assertEquals (23, response.getMappingValueList().size());

        MappingValue baseTicker = new MappingValue ();

        baseTicker.setValue("BASE_TICKER");

        assertEquals (baseTicker, response.getMappingValueList().get (0));

        MappingValue uniqieIdFutOpt = new MappingValue ();

        uniqieIdFutOpt.setValue("UNIQUE_ID_FUT_OPT");

        assertEquals (uniqieIdFutOpt, response.getMappingValueList().get (22));
    }

    @Disabled("This test can cause a '429 Too Many Requests' return code so it's being disabled and should be run manually.")
    @Test
    public void testGetMappingResponseUsingAnInvalidKey () {

        Assertions.assertThrows(BadRequestException.class, () -> {
            MappingResponse response = queryBuilder
                .withExternalApiKey()
                .withValues("Foooooo0000")
            .doGetAsMappingResponse ();
        });
    }

    @Disabled("This test can cause a '429 Too Many Requests' return code so it's being disabled and should be run manually.")
    @Test
    public void testGetWithInvalidIdTypeAndCurrencyFor2005973 () {

        Assertions.assertThrows(BadRequestException.class, () -> {
            Data data = queryBuilder
                .withExternalApiKey()
                .getRequestBody()
                    .newMappingEntry()
                        .withIdType("Foooooooooo00009999")
                        .withIdValue("2005973")
                        .withMarketIdentificationCode("EDGXZZZZZZZZZ")
                        .withCurrency("USDZZZZZZ")
                    .done()
                .done()
            .doGetAsData();
        });
    }

    @Test
    public void testGetWithInvalidRequestIncludingMaturityWhichShouldResultInAnException () {

        Data data = queryBuilder
            .withExternalApiKey()
            .getRequestBody()
                .newMappingEntry()
                    .withIdType("BASE_TICKER")
                    .withIdValue("FG")
                    .withMarketSectorDescription("Mtge")
                    .withSecurityType2("Pool")
                    // Must be no more than one year apart!
                    .withMaturity("2015-09-01", "2018-09-01")
                .done()
            .done()
        .doGetAsData();

        assertNotNull (data);
        assertTrue(1 <= data.getEntries().size());

        /* NOTE: At the moment we're expecting an error but when validation is working client-side we should
         *       see an exception thrown instead.
         */

        List<? extends SerializableBean> dataEntries = data.getEntries().get(0);

        assertTrue (1 == dataEntries.size());

        for (SerializableBean next : dataEntries)
            assertTrue (next instanceof ErrorEntry);
    }
}
