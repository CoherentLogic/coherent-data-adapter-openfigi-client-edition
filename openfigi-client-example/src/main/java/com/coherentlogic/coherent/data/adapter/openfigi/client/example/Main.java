package com.coherentlogic.coherent.data.adapter.openfigi.client.example;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.coherentlogic.coherent.data.adapter.openfigi.client.core.builders.QueryBuilder;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.Data;
import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages="com.coherentlogic.coherent.data.adapter.openfigi")
//@ContextConfiguration(classes={GlobalConfiguration.class, XMLConfiguration.class})
public class Main implements CommandLineRunner {

private static final Logger log = LoggerFactory.getLogger(Main.class);

    @Autowired
    private ApplicationContext applicationContext;

    public static void main(String[] args) throws InterruptedException {

        try {

            SpringApplicationBuilder builder =
                new SpringApplicationBuilder (Main.class);

            builder
                .web(WebApplicationType.NONE)
                .headless(false)
                .registerShutdownHook(true)
                .run(args);

        } catch (Throwable thrown) {
            log.error("ExampleApplication.main caught an exception.", thrown);
        }

        Thread.sleep(Long.MAX_VALUE);

        System.exit(-9999);
    }

    @Override
    public void run(String... arg0) throws Exception {

        Monitor monitor = MonitorFactory.start("Main: get multiple FIGIs");

        List<Data> dataList = new ArrayList<Data> ();

        for (int ctr = 851300; ctr < 851399; ctr++) {

            QueryBuilder queryBuilder = applicationContext.getBean(QueryBuilder.class);

            queryBuilder
                .withExternalApiKey()
                .getRequestBody()
                    .clear()
                    .newMappingEntry()
                        .withIdType("ID_WERTPAPIER")
                        .withIdValue(ctr) // 851399
                    .done()
                .done()
            .doGetAsData(
                data -> {

                    dataList.add(data);

                    return data;
                }
            );
        }

        monitor.stop();

        for (Data data : dataList)
            log.info("data: " + data);

        log.info("monitor: " + monitor);

    }
}
