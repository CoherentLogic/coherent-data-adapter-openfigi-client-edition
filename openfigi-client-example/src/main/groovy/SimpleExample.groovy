@GrabExclude(group = 'org.jboss.spec.javax.ws.rs', module='jboss-jaxrs-api_2.1_spec')
@GrabExclude(group = 'org.jboss.spec.javax.xml.bind', module='jboss-jaxb-api_2.3_spec')
@GrabExclude(group = 'org.jboss.spec.javax.servlet', module='jboss-servlet-api_3.1_spec')
@GrabExclude(group = 'javax.validation', module='validation-api')
@GrabExclude(group = 'org.jboss.spec.javax.annotation', module='jboss-annotations-api_1.2_spec')
@GrabExclude(group = 'net.jcip', module='jcip-annotations')
@GrabExclude(group = 'org.jboss.logging', module='jboss-logging-processor')
@GrabExclude(group = 'org.jboss.logging', module='jboss-logging-annotations')
@GrabExclude(group = 'org.reactivestreams', module='reactive-streams')
@GrabExclude(group = 'javax.activation', module='activation')
@GrabExclude(group = 'junit', module='junit')

@Grab(group='javax.ws.rs', module='javax.ws.rs-api', version='2.1')
@Grab(group='com.jamonapi', module='jamon', version='2.81')
@Grab(group='com.coherentlogic.enterprise-data-adapter', module='aspectj-int', version='2.0.3-RELEASE')
@Grab(group='com.coherentlogic.coherent.data.adapter.openfigi.client', module='openfigi-client-core', version='2.0.3.2-RELEASE')
@Grab(group='com.coherentlogic.coherent.data.adapter.openfigi.client', module='openfigi-client-core', version='2.0.3.2-RELEASE')
@Grab(group='com.coherentlogic.coherent.data.adapter.openfigi.client', module='openfigi-client-configuration', version='2.0.3.2-RELEASE')
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.builders.QueryBuilder

@Grab(group='org.springframework', module='spring-context', version='5.0.7.RELEASE')
import org.springframework.context.annotation.AnnotationConfigApplicationContext

def applicationContext = new AnnotationConfigApplicationContext ("com.coherentlogic.coherent.data.adapter.openfigi.client.core.configuration")

def queryBuilder = applicationContext.getBean(QueryBuilder.class)

def data = queryBuilder
    .withExternalApiKey()
    .getRequestBody()
        .withWertpapier("851399")
    .done()
.doGetAsData();

println "Data: $data"