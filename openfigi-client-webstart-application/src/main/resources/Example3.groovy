/* This example, which is written in Groovy script, sends a request to the OpenFIGI website:
 *
 * https://api.openfigi.com/v1/mapping
 *
 * -----
 *
 * The API key can be set as follows, however it is not necessary to run these examples:
 *
 *     queryBuilder.withApiKey( "[*** ENTER YOUR API KEY HERE ***]" )
 *
 * See here:
 *
 * https://openfigi.com/api#api-key
 */

def data = queryBuilder
    .getRequestBody()
        .withFinancialInstrumentGlobalIdentifier("BBG00K62SK81")
    .done()
.doGetAsData();

return data