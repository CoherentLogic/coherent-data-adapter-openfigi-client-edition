package com.coherentlogic.coherent.data.adapter.openfigi.client.webstart.application;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SplashScreen extends JWindow {

    private static final Logger log = LoggerFactory.getLogger(SplashScreen.class);

    private static final String DEFAULT_CL_LOGO_URL =
        "https://coherentlogic.com/wordpress/wp-content/uploads/2018/01/CoherentLogic_Logo_RBG.png";

    public SplashScreen(Frame frame, int waitTime) {

        super(frame);

        JLabel label;

        try {
            label = new JLabel(
                new ImageIcon(
                    new URL (DEFAULT_CL_LOGO_URL)
                )
            );
        } catch (MalformedURLException malformedURLException) {

            log.error("Unable to create the image icon using the url: " + DEFAULT_CL_LOGO_URL, malformedURLException);

            label = new JLabel("Coherent Logic Limited");
        }

        getContentPane().add(label, BorderLayout.CENTER);

        pack();

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension labelSize = label.getPreferredSize();

        setLocation(screenSize.width / 2 - (labelSize.width / 2), screenSize.height / 2 - (labelSize.height / 2));

        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                setVisible(false);
                dispose();
            }
        });
        final int pause = waitTime;
        final Runnable closerRunner = new Runnable() {
            public void run() {
                setVisible(false);
                dispose();
            }
        };
        Runnable waitRunner = new Runnable() {
            public void run() {
                try {
                    Thread.sleep(pause);
                    SwingUtilities.invokeAndWait(closerRunner);
                } catch (Exception exception) {
                    log.error("An exception was thrown while sleeping and/or trying to close the splash screen.", exception);
                }
            }
        };

        setVisible(true);

        Thread splashThread = new Thread(waitRunner, "SplashThread");

        splashThread.start();
    }
}
