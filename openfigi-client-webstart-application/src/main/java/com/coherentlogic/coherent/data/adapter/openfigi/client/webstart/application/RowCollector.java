package com.coherentlogic.coherent.data.adapter.openfigi.client.webstart.application;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.core.exceptions.GenericReflectionException;
import com.coherentlogic.coherent.data.model.core.annotations.VisitableAnnotationProcessor;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

public class RowCollector implements Consumer<SerializableBean> {

    private static final Logger log = LoggerFactory.getLogger(RowCollector.class);

    private final VisitableAnnotationProcessor visitableAnnotationProcessor = new VisitableAnnotationProcessor ();

    public final String HEADER_ROW = "header";

    public RowCollector() {
        visitableAnnotationProcessor.register("com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain");
    }

    private final Set<String> headers = new LinkedHashSet<String> ();

    private final Collection<Collection<String>> rows = new ArrayList<Collection<String>> ();

    public Collection<Collection<String>> getRows() {

        Collection<Collection<String>> results = new ArrayList<Collection<String>> ();

        results.add(headers);

        rows.forEach(
            row -> {
                results.add(row);
            }
        );

        return results;
    }

    private final List<String> observationsRowValues = new ArrayList<String> ();

    @Override
    public void accept(SerializableBean serializableBean) {

//        Set<Field> fieldSet = visitableAnnotationProcessor.getFieldsFor(serializableBean.getClass());
//
//        if (serializableBean instanceof Observations) {
//            // We do this once and then add the observationsRowValues to all rows added.
//            getValuesFor (fieldSet, serializableBean, observationsRowValues);
//
//        } else if (serializableBean instanceof Observation) {
//
//            log.debug("observationsRowValues: " + observationsRowValues);
//
//            List<String> nextRow = new ArrayList<String> (observationsRowValues);
//
//            getValuesFor (fieldSet, serializableBean, nextRow);
//
//            rows.add(nextRow);
//        }
    }

    void getValuesFor (Set<Field> fieldSet, SerializableBean serializableBean, List<String> nextRow) {

        fieldSet.forEach(
            field -> {

                Object result;

                try {
                    if (!List.class.equals(field.getType())) {

                        field.setAccessible(true);

                        String name = field.getName ();

                        String compositeName = serializableBean.getClass().getName() + "." + name;

                        if (!headers.contains(compositeName))
                            headers.add(compositeName);

                        result = field.get(serializableBean);

                        log.debug("result: " + result);

                        if (result == null) result = "NULL";

                        nextRow.add(result.toString());

                    } else {
                        // Ignore field of type List.
                    }
                } catch (Exception cause) {
                    throw new GenericReflectionException(
                        "Unable to get the value for the field " + field.getName(), cause);
                } finally {
                    field.setAccessible(false);
                }
            }
        );
    }
}
