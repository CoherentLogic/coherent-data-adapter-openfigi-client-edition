package com.coherentlogic.coherent.data.adapter.openfigi.client.webstart.application;

import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.Configuration;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfiguration;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.transaction.LockingMode;
import org.infinispan.transaction.TransactionMode;
import org.infinispan.transaction.lookup.EmbeddedTransactionManagerLookup;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
//@EnableCaching
//@EnableInfinispanEmbeddedHttpSession
public class ApplicationConfiguration {

    public static final String
        OPENFIGI_CACHE_MANAGER = "openFIGICacheManager",
        DEFAULT_CACHE_NAME = "defaultCacheName",
        OPENFIGI_CACHE = "openFIGICache",
        INFINISPAN_DEFAULT_CACHE_FACTORY_BEAN = "infinispanDefaultCacheFactoryBean",
        TRANSACTION_MANAGER = "transactionManager";

    @Bean(name=DEFAULT_CACHE_NAME)
    public String getDefaultCacheName () {
        return OPENFIGI_CACHE;
    }

    /**
     * 
     * @see https://spring.io/blog/2011/08/15/configuring-spring-and-jta-without-full-java-ee/
     * @see https://github.com/jbosstm/quickstart/tree/master/jca-and-hibernate
     */
    @Bean(name=OPENFIGI_CACHE_MANAGER)
    public EmbeddedCacheManager getDefaultCacheManager () {

        // Old return type: import org.springframework.cache.CacheManager;

        GlobalConfigurationBuilder globalConfigurationBuilder =
            GlobalConfigurationBuilder.defaultClusteredBuilder();

        GlobalConfiguration globalConfiguration =
            globalConfigurationBuilder
                .transport()
                .addProperty("configurationFile", "infinispan-default-jgroups-tcp.xml")
                .clusterName("OpenFIGIClientCluster")
                .defaultTransport()
                .defaultCacheName("OpenFIGIClientCache")
                .build();

        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder ();

        Configuration configuration = configurationBuilder
            .transaction()
                .transactionMode(TransactionMode.TRANSACTIONAL)
                .lockingMode(LockingMode.PESSIMISTIC)
                .autoCommit(false)
                 // https://docs.jboss.org/infinispan/9.0/apidocs/org/infinispan/transaction/lookup/TransactionManagerLookup.html
                 // EmbeddedTransactionManagerLookup
                .transactionManagerLookup(new EmbeddedTransactionManagerLookup ())//GenericTransactionManagerLookup ())
            .jmxStatistics()
            .clustering()
               .cacheMode(CacheMode.DIST_SYNC)
               .l1()
                .lifespan(60000L * 5L) // Five minutes
            .build();

        DefaultCacheManager result = new DefaultCacheManager(globalConfiguration, configuration);

        result.addListener(new OpenFIGIClusterAndCacheListener ());

        // https://github.com/hantsy/spring4-sandbox/wiki/cache-infinispan-embedded
        return result;//new SpringEmbeddedCacheManager (result);
    }

//    /**
//     * @see 21.8.4 "Externalizing session using Spring Session" here:
//     *      http://infinispan.org/docs/stable/user_guide/user_guide.html
//     */
//    @Bean(name=INFINISPAN_DEFAULT_CACHE_FACTORY_BEAN)
//    public InfinispanDefaultCacheFactoryBean<String, SerializableBean>
//        getInfinispanDefaultCacheFactoryBean (@Qualifier (OPENFIGI_CACHE_MANAGER) EmbeddedCacheManager cacheManager
//    ) {
//
//        CacheContainer cacheContainer = cacheManager.getNativeCacheManager();
//
//        InfinispanDefaultCacheFactoryBean<String, SerializableBean> result = new InfinispanDefaultCacheFactoryBean<String, SerializableBean> ();
//
//        // If this step is skipped, well see the following in the log "// No Infinispan CacheContainer has been set"
//        result.setInfinispanCacheContainer(cacheContainer);
//
//        return result;
//        
//    }
}
