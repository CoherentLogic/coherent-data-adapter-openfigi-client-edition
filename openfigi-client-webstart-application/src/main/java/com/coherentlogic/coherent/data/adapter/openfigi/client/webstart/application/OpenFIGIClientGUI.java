package com.coherentlogic.coherent.data.adapter.openfigi.client.webstart.application;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.coherentlogic.coherent.data.adapter.core.exceptions.IORuntimeException;
import com.coherentlogic.coherent.data.adapter.core.exceptions.InvalidURIException;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.builders.QueryBuilder;
import com.jamonapi.MonKey;
import com.jamonapi.MonKeyImp;
import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;



/**
 * The front-end for the OpenFIGI Client that allows users to directly work with the
 * {@link com.coherentlogic.coherent.data.adapter.openfigi.client.core.builders.QueryBuilder}. 
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages= {"com.coherentlogic.coherent.data.adapter.openfigi"}) // , "com.coherentlogic.coherent.data.adapter.openfigi.client"
public class OpenFIGIClientGUI extends JFrame implements CommandLineRunner {

    private static final long serialVersionUID = 1L;

    private static final Logger log = LoggerFactory.getLogger(OpenFIGIClientGUI.class);

    private static final String USER_HOME = System.getProperty("user.home");

    private URI uri = null;

    private static final String EXAMPLE_1 = "Example 1", EXAMPLE_2 = "Example 2";

    private static final String EXAMPLE_1_QUERY_BUILDER_FACTORY = "example1QueryBuilderFactory",
        EXAMPLE_2_QUERY_BUILDER_FACTORY = "example2QueryBuilderFactory";

    private static final Map<String, String> beanIdMap =
        new HashMap<String, String> ();

    static {
        beanIdMap.put(EXAMPLE_1, EXAMPLE_1_QUERY_BUILDER_FACTORY);
        beanIdMap.put(EXAMPLE_2, EXAMPLE_2_QUERY_BUILDER_FACTORY);
    }

    private static final String
        QUERY_BUILDER = "queryBuilder",
        LOG = "log";

    private final JTextArea outputTextArea = new JTextArea();

    private final JButton runScriptButton = new JButton("Run script");

    private final ButtonGroup requestMenuItemsGroup = new ButtonGroup();

    private final Map<ButtonModel, JRadioButtonMenuItem> radioButtonMap =
        new HashMap<ButtonModel, JRadioButtonMenuItem> ();

    private GroovyEngine groovyEngine;

//    private Map<String, QueryBuilderFactory> queryBuilderFactoryMap;

    private Map<String, String> exampleMap;

    @Autowired
    private ApplicationContext applicationContext;

    private final ObjectStringifier objectStringifier =
        new ObjectStringifier ();

    private final MonKey monKey = new MonKeyImp(
        "Call OpenFIGI web services and return an instance of a domain class.",
        TimeUnit.MILLISECONDS.toString());

    /**
     * @see #initialize()
     */
    void initializeMenu (JTextArea inputTextArea) {
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

//        JMenu mnFile = new JMenu("File");
//        menuBar.add(mnFile);
//
//        JMenuItem mntmExit = new JMenuItem("Exit");
//        mnFile.add(mntmExit);

        JMenu mnRequest = new JMenu("Examples");
        menuBar.add(mnRequest);

        JRadioButtonMenuItem example_1 = new JRadioButtonMenuItem("Example 1");
        example_1.setSelected(true);
        mnRequest.add(example_1);

        JRadioButtonMenuItem example_2 = new JRadioButtonMenuItem("Example 2");
        mnRequest.add(example_2);

        example_1.addActionListener(new MenuItemSelectedActionListener (exampleMap, inputTextArea));
        example_2.addActionListener(new MenuItemSelectedActionListener (exampleMap, inputTextArea));

        requestMenuItemsGroup.add(example_1);
        requestMenuItemsGroup.add(example_2);

        radioButtonMap.put(example_1.getModel(), example_1);
        radioButtonMap.put(example_2.getModel(), example_2);

        addHelpAbout (menuBar);
    }

    private void addHelpAbout (JMenuBar menuBar) {
        JMenu helpMenu = new JMenu("Help");
        menuBar.add(helpMenu);

        addAPIMenuItem (helpMenu);

        JMenuItem mntmAbout = new JMenuItem("About");
        helpMenu.add(mntmAbout);

        mntmAbout.addActionListener(
            new ActionListener () {

                @Override
                public void actionPerformed(ActionEvent actionEvent) {

                    AboutDialog dialog;

                    try {
                        dialog = new AboutDialog ();
                    } catch (IOException ioException) {
                        throw new IORuntimeException(
                            "Unable to create the AboutDialog.", ioException);
                    }
                    dialog.setVisible(true);
                }
            }
        );
    }

    private void addAPIMenuItem (JMenu helpMenu) {

        final String destination = "http://bit.ly/2CkzBZm";

        JMenuItem apiJavadocs = new JMenuItem("API Javadocs");

        apiJavadocs.setForeground(Color.blue);

        helpMenu.add(apiJavadocs);

        apiJavadocs.addActionListener(
            new ActionListener () {

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    // This is for tracking purposes and will direct the user
                    // to http://coherentlogic.com/fredJavaDoc/
                    try {
                        AboutDialog.open(destination);
                    } catch (URISyntaxException uriSyntaxException) {
                        throw new InvalidURIException(
                            "Unable to open the destination: " + destination,
                            uriSyntaxException
                        );
                    }
                }
            }
        );
    }

    /**
     * Method configures the Swing components that are added to this object's
     * JFrame.
     */
    @PostConstruct
    public void initialize () {

        groovyEngine = applicationContext.getBean(GroovyEngine.class);

        exampleMap = (Map<String, String>) applicationContext.getBean("exampleMap");

        try {
            uri = new URI("https://twitter.com/CoherentMktData");
        } catch (URISyntaxException uriSyntaxException) {
            throw new RuntimeException (uriSyntaxException);
        }

        setTitle("OpenFIGI Client GUI");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final JPanel parent = new JPanel();
        parent.setLayout(new BorderLayout()); 

        final JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));

        parent.add(panel);

        getContentPane().add(parent, BorderLayout.CENTER);
        setExtendedState(Frame.MAXIMIZED_BOTH); 

        JLabel enterYourQueryLabel = new JLabel(
            "Enter your query here (context contains references to: queryBuilder):");

        panel.add(enterYourQueryLabel);

        final JTextArea inputTextArea = new JTextArea();

        JScrollPane inputTextAreaScrollPane = new JScrollPane(inputTextArea);

        inputTextAreaScrollPane.
            setVerticalScrollBarPolicy(
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        inputTextAreaScrollPane.
        setHorizontalScrollBarPolicy(
            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        initializeMenu(inputTextArea);

        String exampleText = exampleMap.get (EXAMPLE_1);

        inputTextArea.setText(exampleText);

        inputTextArea.setColumns(80);
        inputTextArea.setRows(40);
        panel.add(inputTextAreaScrollPane);

        panel.add(runScriptButton);

        JLabel outputAppearsBelowLabel = new JLabel("The output appears below:");

        panel.add(outputAppearsBelowLabel);

        outputTextArea.setColumns(80);
        outputTextArea.setRows(40);

        JScrollPane outputTextAreaScrollPane = new JScrollPane(outputTextArea);

        outputTextAreaScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        panel.add(outputTextAreaScrollPane);

        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();

        Rectangle bounds = env.getMaximumWindowBounds();

        setBounds(bounds);

        runScriptButton.addActionListener(
            new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent actionEvent) {

                    String scriptText = inputTextArea.getText();

                    log.info("scriptText:\n\n" + scriptText);

                    ButtonModel buttonModel = requestMenuItemsGroup.getSelection();

                    JRadioButtonMenuItem selectedMenuItem = radioButtonMap.get(buttonModel);

                    log.info("buttonModel: " + buttonModel);

//                    String selectedText = selectedMenuItem.getText();

//                    QueryBuilderFactory queryBuilderFactory =
//                        (QueryBuilderFactory)
//                            queryBuilderFactoryMap.get(selectedText);
//
//                    QueryBuilder requestBuilder =
//                        queryBuilderFactory.getInstance();
//
//                    CategoriesRepository categoriesDAO = applicationContext.getBean(CategoriesRepository.class);

                    QueryBuilder queryBuilder = applicationContext.getBean(QueryBuilder.class);

//                    queryBuilder.addQueryBuilderEventListener(
//                        queryBuilderEvent -> {
//                            log.info("queryBuilderEvent.key: " + queryBuilderEvent.getKey() +
//                                ", eventType: " + queryBuilderEvent.getEventType());
//                        }
//                    );

//                    String apiKey = (String) applicationContext.getBean("openFIGIApiKey");
//
//                    queryBuilder.withApiKey(apiKey);

                    groovyEngine.setVariable(QUERY_BUILDER, queryBuilder);
                    groovyEngine.setVariable(LOG, log);
//                    groovyEngine.setVariable(CATEGORIES_SERVICE, categoriesDAO);

                    Object result = null;

                    Monitor monitor = MonitorFactory.start(monKey);

                    try {
                        result = groovyEngine.evaluate(scriptText);
                    } catch (Throwable throwable) {

                        log.error("Evaluation failed for the script:\n\n" + scriptText, throwable);

                        JOptionPane.showMessageDialog(
                            panel,
                            throwable.getMessage(),
                            "Evaluation failed!",
                            JOptionPane.ERROR_MESSAGE);

                        return;
                    } finally {
                        monitor.stop();
                        log.info ("JAMon report: " + monitor);
                    }

                    log.debug("result: " + result);

                    if (result != null) {

                        String stringifiedResult = objectStringifier.toString(result);

                        String fullResult =
                            "// Note that null values are not indicative of a problem, per se, for \n" +
                            "// example the PrimaryKey is only ever assigned when the object has been \n" +
                            "// saved to a database and since this does not happen in this example.\n" +
                            "//\n" +
                            "// -----\n" +
                            "//\n" +
                            "// JAMON Performance Metrics:\n" +
                            "//\n" +
                            "// " + monitor + "\n" +
                            "//\n" +
                            "// -----\n" +
                            "//\n\n\n" +
                            stringifiedResult;

                        outputTextArea.setText(fullResult);
                    }
                }
            }
        );
    }

    /**
     * The main method uses the Spring application context to get an instance of
     * {@link OpenFIGIClientGUI} and then displays this object.
     */
    @Override
    public void run(String... args) throws Exception {

        // Server server = Server.createTcpServer("-tcpAllowOthers","-webAllowOthers").start();
//
//        String url = server.getURL();
//
//        log.info("The h2 URL is: " + url);

        SplashScreen splashScreen = new SplashScreen (this, 3000);

        setVisible(true);

        java.awt.EventQueue.invokeLater(
            () -> {
                toFront();
                repaint();
            }
        );
    }

    public static void main (String[] unused) throws InterruptedException {

        try {

            SpringApplicationBuilder builder =
                new SpringApplicationBuilder (OpenFIGIClientGUI.class);

            builder
                .web(false)
                .headless(false)
                .registerShutdownHook(true)
                .run(unused);

        } catch (Throwable thrown) {
            log.error("ExampleApplication.main caught an exception.", thrown);
        }

        Thread.sleep(Long.MAX_VALUE);

        System.exit(-9999);
    }
}

/**
 * An {@link java.awt.event ActionListener} implementation that adds a given
 * example to the inputTextArea when the user selects a given
 * {@link javax.swing.JMenuItem}.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
class MenuItemSelectedActionListener implements ActionListener {

    private final Map<String, String> exampleMap;

    private final JTextArea inputTextArea;

    public MenuItemSelectedActionListener(
        Map<String, String> exampleMap,
        JTextArea inputTextArea
    ) {
        super();
        this.exampleMap = exampleMap;
        this.inputTextArea = inputTextArea;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {

        JMenuItem menuItem = (JMenuItem) actionEvent.getSource();

        String selectedMenu = menuItem.getText();

        String example = exampleMap.get(selectedMenu);

        inputTextArea.setText(example);
    }
}
