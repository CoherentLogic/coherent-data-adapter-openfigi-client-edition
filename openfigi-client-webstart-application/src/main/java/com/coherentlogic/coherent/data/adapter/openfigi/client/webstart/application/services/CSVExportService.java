package com.coherentlogic.coherent.data.adapter.openfigi.client.webstart.application.services;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

public class CSVExportService {

    public void export (Collection<Collection<String>> records, OutputStream out) throws IOException {

        CSVPrinter csvPrinter = new CSVPrinter(new OutputStreamWriter (out), CSVFormat.DEFAULT);

        Optional.of(records).ifPresent(
            _records -> {

                _records.forEach(
                    record -> {

                        try {
                            csvPrinter.printRecord(record); 
                        } catch (Exception e) {
                            throw new RuntimeException ("Failed to print records for the records: " + records +
                                " to the output stream: " + out, e);
                        }
                    }
                );
            }
        );

        csvPrinter.flush();
        csvPrinter.close();
    }

    public List<List<String>> fromColumns (Map<String, List<String>> columns) {

        List<List<String>> rows = new ArrayList<List<String>> ();

        columns.forEach(
            (String key, List<String> column) -> {
                
                List<String> row = new ArrayList<String> ();
                
                int columnCtr = 0, rowCtr = 0;
                
            }
        );

        return rows;

    }

    /* Keep track of the row number and the column number
     * call nextRow, pass in the ctrs and the row, and the columns
     * for each header, get the column@columnCtr and add that to the row.
     *     if value == null : NULL / value
     */
    
    public void fromColumns (Map<String, List<String>> columns, List<String> row, int rowCtr) {

        columns.entrySet().forEach(
            entry -> {

                String entryKey = entry.getKey();

                List<String> values = entry.getValue();

                String value = values.get(rowCtr);

                row.add(value);
                
            }
        );
    }
}
