/**
 * This package contains classes that facilitate the OpenFIGI Client graphical user interface.
 */
package com.coherentlogic.coherent.data.adapter.openfigi.client.webstart.application;