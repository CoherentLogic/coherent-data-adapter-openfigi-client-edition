package com.coherentlogic.coherent.data.adapter.openfigi.client.webstart.application;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.core.exceptions.InvalidURIException;

/**
 * 
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class AboutDialog extends JDialog {

    private static final Logger log = LoggerFactory.getLogger(AboutDialog.class);

    private static final String TWITTER_URL = "https://twitter.com/CoherentMktData";

    public AboutDialog() throws IOException {
        init();
    }

    private static JLabel newLabel (String text) {
        JLabel result = new JLabel(text);
        result.setFont(new Font("Arial", Font.BOLD, 13));
        result.setAlignmentX(0.5f);

        return result;
    }

    public static void open(String uri) throws URISyntaxException {
        open (new URI (uri));
    }

    private static void open(URI uri) {
        if (Desktop.isDesktopSupported()) {
            try {
                Desktop.getDesktop().browse(uri);
            } catch (IOException ioException) {
                throw new RuntimeException(
                    "Unable to open the uri " + uri, ioException);
            }
        } else {
            log.warn("The desktop is not supported so the uri '" + uri +
                "' will not be browsed.");
        }
    }

    public final void init() throws IOException {

        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        add(Box.createRigidArea(new Dimension(0, 10)));

        final ImageIcon icon = new ImageIcon(new URL ("https://coherentlogic.com/wordpress/wp-content/uploads/2018/02/OpenFIGI-Client_Logo.jpg"));
        JLabel iconLabel = new JLabel(icon);
        iconLabel.setAlignmentX(0.5f);

        add(Box.createRigidArea(new Dimension(0, 10)));

        JLabel blank = newLabel("");

        JLabel name = newLabel(
            "Coherent Logic OpenFIGI Client GUI version 3.0.1-RELEASE");
        JLabel copyright = newLabel(
            "Copyright (C) 2012 - Present Coherent Logic Limited; All Rights Reserved.");
        JLabel license = newLabel(
            "Licensed under the GNU Lesser General Public License");
        JLabel follow = newLabel("Follow us on Twitter at:");

        JLabel link = newLabel(TWITTER_URL);
        link.setForeground(Color.blue);

        link.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        link.addMouseListener(
            new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    try {
                        open (TWITTER_URL);
                    } catch (URISyntaxException uriSyntaxException) {
                        throw new InvalidURIException (
                            "The uri '" + TWITTER_URL + "' could not be opened.", uriSyntaxException);
                    }
                }
            }
        );

//        JPanel panel = new JPanel () {
//
//            final ImageIcon backgroundImage = icon;
//
//            @Override
//            protected void paintComponent(Graphics graphics) {
//                super.paintComponent(graphics);
//                graphics.drawImage(backgroundImage.getImage(), 0, 0, getWidth(), getHeight(), this);
//            }
//
//            @Override
//            public Dimension getPreferredSize() {
//                Dimension size = super.getPreferredSize();
//                size.width = Math.max(backgroundImage.getIconWidth(), size.width);
//                size.height = Math.max(backgroundImage.getIconHeight(), size.height);
//
//                return size;
//            }
//        };
//
//        add (panel);

        add(iconLabel);
        add(name);
        add(license);
        add(copyright);
        add(blank);
        add(follow);
        add(link);

        add(Box.createRigidArea(new Dimension(0, 50)));

        JButton close = new JButton("Close");
        close.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent event) {
                dispose();
            }
        });

        close.setAlignmentX(0.5f);
        add(close);

        setModalityType(ModalityType.APPLICATION_MODAL);

        setTitle("About the OpenFIGI Client GUI");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
//        setLocationRelativeTo(null);
        pack();
        setLocationByPlatform(true);
        setSize(750, 750);
    }
}
