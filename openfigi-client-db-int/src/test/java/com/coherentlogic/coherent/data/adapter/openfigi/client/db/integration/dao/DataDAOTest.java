package com.coherentlogic.coherent.data.adapter.openfigi.client.db.integration.dao;

import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import com.coherentlogic.coherent.data.adapter.openfigi.client.core.builders.QueryBuilder;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.configuration.GlobalConfiguration;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.Data;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.DataEntry;
import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.ErrorEntry;
import com.coherentlogic.coherent.data.adapter.openfigi.client.db.integration.services.DataEntryService;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * Unit test for the {@link DataEntryRepository} class.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@ExtendWith(SpringExtension.class)
@Rollback
@Transactional
@ContextConfiguration(classes={GlobalConfiguration.class, TestXMLConfiguration.class})
public class DataDAOTest {

    @Autowired
    private DataEntryService dataEntryDAO;

    @Autowired
    private ErrorEntryRepository errorEntryDAO;

    @Autowired
    private QueryBuilder queryBuilder;

    @AfterEach
    public void tearDown() throws Exception {
        dataEntryDAO = null;
        errorEntryDAO = null;
        queryBuilder = null;
    }

    @Test
    public void reviewCRUDOperations () {
try{
        Data data = queryBuilder
            .withExternalApiKey()
            .getRequestBody()
                .newMappingEntry()
                    .withIdType("ID_ISIN")
                    .withIdValue("US4592001014")
                .done()
                .newMappingEntry()
                    .withIdType("ID_WERTPAPIER")
                    .withIdValue("851399")
                .done()
                .newMappingEntry()
                    .withIdType("ID_WERTPAPIER")
                    .withIdValue("INVALID")
                .done()
            .done()
        .doGetAsData();

        // See also ScopedBeanInterceptor

        for (List<? extends SerializableBean> nextEntries : data.getEntries()) {
            if (0 < nextEntries.size()) {
                if (nextEntries.get(0) instanceof DataEntry) {
                    dataEntryDAO.save((Collection<DataEntry>) nextEntries);
                } else {
                    errorEntryDAO.saveAll((Collection<ErrorEntry>) nextEntries);
                }
            }
        }
} catch (Throwable ttt) { ttt.printStackTrace(); }
    }
}
