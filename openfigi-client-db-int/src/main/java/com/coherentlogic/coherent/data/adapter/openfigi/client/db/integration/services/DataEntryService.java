package com.coherentlogic.coherent.data.adapter.openfigi.client.db.integration.services;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.DataEntry;
import com.coherentlogic.coherent.data.adapter.openfigi.client.db.integration.dao.DataEntryRepository;

@Repository(DataEntryService.BEAN_NAME)
@Transactional
public class DataEntryService {

    public static final String BEAN_NAME = "dataEntryService";

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private DataEntryRepository dataEntryRepository;

    public <S extends DataEntry> S save(S entity) {
        return dataEntryRepository.save(entity);
    }

    public <S extends DataEntry> Optional<S> findOne(Example<S> example) {
        return dataEntryRepository.findOne(example);
    }

    public Page<DataEntry> findAll(Pageable pageable) {
        return dataEntryRepository.findAll(pageable);
    }

    public List<DataEntry> findAll() {
        return dataEntryRepository.findAll();
    }

    public Optional<DataEntry> findOne(Long id) {
        return dataEntryRepository.findById(id);
    }

    public List<DataEntry> findAll(Sort sort) {
        return dataEntryRepository.findAll(sort);
    }

    public List<DataEntry> findAll(Iterable<Long> ids) {
        return dataEntryRepository.findAllById(ids);
    }

    public boolean exists(Long id) {
        return dataEntryRepository.existsById(id);
    }

    public <S extends DataEntry> List<S> save(Iterable<S> entities) {
        return dataEntryRepository.saveAll(entities);
    }

    public void flush() {
        dataEntryRepository.flush();
    }

    public <S extends DataEntry> S saveAndFlush(S entity) {
        return dataEntryRepository.saveAndFlush(entity);
    }

    public long count() {
        return dataEntryRepository.count();
    }

    public void deleteInBatch(Iterable<DataEntry> entities) {
        dataEntryRepository.deleteInBatch(entities);
    }

    public <S extends DataEntry> Page<S> findAll(Example<S> example, Pageable pageable) {
        return dataEntryRepository.findAll(example, pageable);
    }

    public void delete(Long id) {
        dataEntryRepository.deleteById(id);
    }

    public void deleteAllInBatch() {
        dataEntryRepository.deleteAllInBatch();
    }

    public void delete(DataEntry entity) {
        dataEntryRepository.delete(entity);
    }

    public DataEntry getOne(Long id) {
        return dataEntryRepository.getOne(id);
    }

    public <S extends DataEntry> long count(Example<S> example) {
        return dataEntryRepository.count(example);
    }

    public void delete(Iterable<? extends DataEntry> entities) {
        dataEntryRepository.deleteAll(entities);
    }

    public <S extends DataEntry> List<S> findAll(Example<S> example) {
        return dataEntryRepository.findAll(example);
    }

    public void deleteAll() {
        dataEntryRepository.deleteAll();
    }

    public <S extends DataEntry> boolean exists(Example<S> example) {
        return dataEntryRepository.exists(example);
    }

    public <S extends DataEntry> List<S> findAll(Example<S> example, Sort sort) {
        return dataEntryRepository.findAll(example, sort);
    }
}
