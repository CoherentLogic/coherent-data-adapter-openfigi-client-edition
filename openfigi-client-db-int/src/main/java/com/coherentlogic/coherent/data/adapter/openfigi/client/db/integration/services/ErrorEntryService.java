package com.coherentlogic.coherent.data.adapter.openfigi.client.db.integration.services;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.ErrorEntry;
import com.coherentlogic.coherent.data.adapter.openfigi.client.db.integration.dao.ErrorEntryRepository;

@Repository(ErrorEntryService.BEAN_NAME)
@Transactional
public class ErrorEntryService {

    public static final String BEAN_NAME = "errorEntryService";

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ErrorEntryRepository errorEntryRepository;

    public <S extends ErrorEntry> S save(S entity) {
        return errorEntryRepository.save(entity);
    }

    public <S extends ErrorEntry> Optional<S> findOne(Example<S> example) {
        return errorEntryRepository.findOne(example);
    }

    public Page<ErrorEntry> findAll(Pageable pageable) {
        return errorEntryRepository.findAll(pageable);
    }

    public List<ErrorEntry> findAll() {
        return errorEntryRepository.findAll();
    }

    public Optional<ErrorEntry> findOne(Long id) {
        return errorEntryRepository.findById(id);
    }

    public List<ErrorEntry> findAll(Sort sort) {
        return errorEntryRepository.findAll(sort);
    }

    public List<ErrorEntry> findAll(Iterable<Long> ids) {
        return errorEntryRepository.findAllById(ids);
    }

    public boolean exists(Long id) {
        return errorEntryRepository.existsById(id);
    }

    public <S extends ErrorEntry> List<S> save(Iterable<S> entities) {
        return errorEntryRepository.saveAll(entities);
    }

    public void flush() {
        errorEntryRepository.flush();
    }

    public <S extends ErrorEntry> S saveAndFlush(S entity) {
        return errorEntryRepository.saveAndFlush(entity);
    }

    public long count() {
        return errorEntryRepository.count();
    }

    public void deleteInBatch(Iterable<ErrorEntry> entities) {
        errorEntryRepository.deleteInBatch(entities);
    }

    public <S extends ErrorEntry> Page<S> findAll(Example<S> example, Pageable pageable) {
        return errorEntryRepository.findAll(example, pageable);
    }

    public void delete(Long id) {
        errorEntryRepository.deleteById(id);
    }

    public void deleteAllInBatch() {
        errorEntryRepository.deleteAllInBatch();
    }

    public void delete(ErrorEntry entity) {
        errorEntryRepository.delete(entity);
    }

    public ErrorEntry getOne(Long id) {
        return errorEntryRepository.getOne(id);
    }

    public <S extends ErrorEntry> long count(Example<S> example) {
        return errorEntryRepository.count(example);
    }

    public void delete(Iterable<? extends ErrorEntry> entities) {
        errorEntryRepository.deleteAll(entities);
    }

    public <S extends ErrorEntry> List<S> findAll(Example<S> example) {
        return errorEntryRepository.findAll(example);
    }

    public void deleteAll() {
        errorEntryRepository.deleteAll();
    }

    public <S extends ErrorEntry> boolean exists(Example<S> example) {
        return errorEntryRepository.exists(example);
    }

    public <S extends ErrorEntry> List<S> findAll(Example<S> example, Sort sort) {
        return errorEntryRepository.findAll(example, sort);
    }
}
