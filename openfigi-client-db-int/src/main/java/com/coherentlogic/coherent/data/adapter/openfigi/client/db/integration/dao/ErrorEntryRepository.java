package com.coherentlogic.coherent.data.adapter.openfigi.client.db.integration.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.ErrorEntry;

/**
 * Data access pattern implementation for {@link ErrorEntry} objects.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Transactional
public interface ErrorEntryRepository extends JpaRepository<ErrorEntry, Long> {

    public static final String ERROR_ENTRY_DAO = "errorEntryDAO";
}
