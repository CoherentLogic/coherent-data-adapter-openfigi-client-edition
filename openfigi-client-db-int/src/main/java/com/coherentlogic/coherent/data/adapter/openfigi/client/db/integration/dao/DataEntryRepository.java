package com.coherentlogic.coherent.data.adapter.openfigi.client.db.integration.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.coherentlogic.coherent.data.adapter.openfigi.client.core.domain.DataEntry;

/**
 * Data access pattern implementation for DataEntry objects.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Transactional
public interface DataEntryRepository extends JpaRepository<DataEntry, Long> {

    public static final String DATA_ENTRY_DAO = "dataEntryDAO";
}
